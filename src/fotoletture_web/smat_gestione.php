<?php

$pg="gestione.php";
$limite_inferiore_lavorazioni="0";
//session_start();
if (!isset($_SESSION["connesso"])){session_start(); }

if(isset($_SESSION["filtro_show_cLocalita"])){$cLocalita=$_SESSION["filtro_show_cLocalita"];}

//echo "conn=".$_SESSION["connesso"];
if ($_SESSION["connesso"]=="no"){require_once("index.php"); die; }
require_once("funzioni.php");


$fase="";
if (isset($_POST["fase"])){$fase=CLEAN_VAR($_POST["fase"]);}
if (isset($_GET["fase"])){$fase=CLEAN_VAR($_GET["fase"]);}

$file_id="";
if (isset($_POST["file_id"])){$file_id=CLEAN_VAR($_POST["file_id"]);}

$checked="";
if (isset($_POST["checked"])){$checked=CLEAN_VAR($_POST["checked"]);}

$eye_value="";
if (isset($_GET["eye_value"])){$eye_value=CLEAN_VAR($_GET["eye_value"]);}


// ___________________________________________________________ FILTRO GIRI COMUNE
if (isset($_GET["giri_comune"])){
	
	$buff=str_replace("^", "''", CLEAN_VAR($_GET["giri_file"]));
	if ($buff==$_SESSION["filtro_giri_file"]){$_SESSION["filtro_giri_file"]="";}else{$_SESSION["filtro_giri_file"]=$buff;}
	$_SESSION["file_pagina"]=1;
}
$q_giri_comune="";
if (strlen($_SESSION["filtro_giri_file"])>0){
	$giri_file=$_SESSION["filtro_giri_file"];
  $q_giri_comune= " and substring(b.caricofile_nome, 5, 3)='".substr($giri_file, 4, 3)."' "; 
	//$q_giri_comune=" and b.caricofile_nome='".$_SESSION["filtro_giri_comune"]."' ";
	if ($fase=="giri_comune"){$_SESSION["file_pagina"]=1;}
	//die ($q_giri_comune);
}

// ___________________________________________________________ FILTRO GIRI GIRO
if (isset($_GET["giri_giro"])){
	$buff=CLEAN_VAR($_GET["giri_giro"]);
	if ($buff==$_SESSION["filtro_giri_giro"]){$_SESSION["filtro_giri_giro"]="";}else{$_SESSION["filtro_giri_giro"]=$buff;}
	$_SESSION["file_pagina"]=1;
}
$q_giri_giro="";
if (strlen($_SESSION["filtro_giri_giro"])>0){
	$q_giri_giro=" and trim(substring(b.caricofile_nome, 8, 3))='".$_SESSION["filtro_giri_giro"]."' ";
	if ($fase=="giri_giro"){$_SESSION["file_pagina"]=1;}
}

// ___________________________________________________________ FILTRO GIRI PERIODO
if (isset($_GET["giri_periodo"])){
	$buff=CLEAN_VAR($_GET["giri_periodo"]);
	if ($buff==$_SESSION["filtro_giri_periodo"]){$_SESSION["filtro_giri_periodo"]="";}else{$_SESSION["filtro_giri_periodo"]=$buff;}
	$_SESSION["file_pagina"]=1;
}
$q_giri_periodo="";
if (strlen($_SESSION["filtro_giri_periodo"])>0){
	$q_giri_periodo=" and trim(substring(a.extra, 1, 6))='".$_SESSION["filtro_giri_periodo"]."' ";
	if ($fase=="giri_periodo"){$_SESSION["file_pagina"]=1;}
}



require_once("header.php");

   if (strlen($_SESSION["file_righe_per_pagina"])==0){$_SESSION["file_righe_per_pagina"]=25;}
   if (strlen($_SESSION["file_pagina"])==0){$_SESSION["file_pagina"]=1;}
   


   $utilizzatore_alias=$_SESSION["ut-nome"];
 	 $utilizzatore_id=$_SESSION["ud-id"];
 	 $utilizzatore_ruolo=$_SESSION["ut-ruolo"];
 	 $utilizzatore_connesso=$_SESSION["connesso"];
 	 
 	 if (strpos($_SESSION["localita"], "TORINO")!==false){$_SESSION["localita"]="TORINO";}
   if (strpos($_SESSION["localita"], "RIVOLI")!==false){$_SESSION["localita"]="RIVOLI";}	 
 	 
 	 
 	 // __________________________________________________________________________________________________________________________FILTRI
	if (isset($_POST['pdr'])){$_SESSION["pdr"]=StripTrim(CLEAN_VAR($_POST['pdr']));}
	if (isset($_POST['matricola'])){$_SESSION["matricola"]=StripTrim(CLEAN_VAR($_POST['matricola']));}
	if (isset($_POST['nominativo'])){$_SESSION["nominativo"]=StripTrim(CLEAN_VAR($_POST['nominativo']));}
	if (isset($_POST['codiceutente'])){$_SESSION["codiceutente"]=StripTrim(CLEAN_VAR($_POST['codiceutente']));}
	if (isset($_POST['flag'])){$_SESSION["flag"]=StripTrim(CLEAN_VAR($_POST['flag']));}
	if (isset($_POST['esito'])){$_SESSION["esito"]=StripTrim(CLEAN_VAR($_POST['esito']));}
	if (isset($_POST['acc'])){$_SESSION["acc"]=StripTrim(CLEAN_VAR($_POST['acc']));}
	if (isset($_POST['controlli'])){$_SESSION["controlli"]=StripTrim(CLEAN_VAR($_POST['controlli']));}
	if (isset($_POST['note'])){$_SESSION["note"]=StripTrim(CLEAN_VAR($_POST['note']));}
	if (isset($_POST['localita'])){$_SESSION["localita"]=StripTrim(CLEAN_VAR($_POST['localita']));}
	if (isset($_POST['comune'])){$_SESSION["comune"]=StripTrim(CLEAN_VAR($_POST['comune']));}
	if (isset($_POST['file'])){$_SESSION["file"]=StripTrim(CLEAN_VAR($_POST['file']));}
	
// ________________________________________________________________________________________________________________________________	

//echo "Pagina= ".$_SESSION["pagina"].'   RIghe per pagine= '.$_SESSION["righe_per_pagina"].'  Pdr='.$_SESSION["pdr"].'   Post PDR='.CLEAN_VAR($_POST['pdr']);



// _______________________________________________________________________________________________________________ RIMOZIONE FILTRI
  if ($fase=="leva_filtri"){
	  $_SESSION["pdr"]="";
		$_SESSION["matricola"]="";
		$_SESSION["nominativo"]="";
		$_SESSION["codiceutente"]="";
		$_SESSION["flag"]="";
		$_SESSION["esito"]="";
		$_SESSION["acc"]="";
		$_SESSION["controlli"]="";
		$_SESSION["note"]="";
		$_SESSION["localita"]="";
		$_SESSION["comune"]="";
		$_SESSION["file"]="";
	}	
	//echo "Session Localita [".$_SESSION["localita"]."]";
// ________________________________________________________________________________________________________________________________






// ______________________________________________________________________________________________________________________ VARIABILI
	$pdr=$_SESSION["pdr"];
	$matricola=$_SESSION["matricola"];
	$nominativo=$_SESSION["nominativo"];
	$codiceutente=$_SESSION["codiceutente"];
	$flag=$_SESSION["flag"];
	$esito=$_SESSION["esito"];
	$acc=$_SESSION["acc"];
	$controlli=$_SESSION["controlli"];
	$note=$_SESSION["note"];
	$localita=$_SESSION["localita"];
	$comune=$_SESSION["comune"];
	$file=$_SESSION["file"];
	
// __________________________________________________________________________________________________________________________________


   



// ____________________________________________________________________________________________________________________________ QUERY
	$QUERY="";
	
	// ATTENZIONE!!! Solo per OTILS solo per AGSM il PDR � afanumerico dentro Extra
	
	if ($_SESSION["ut-ente"]==7){
	  if (strlen($pdr)>0) {$QUERY.=" and substring(extra,21,".strlen($pdr).")=$pdr ";}
	} else {
		if (strlen($pdr)>0) {$QUERY.=" and substring(pdr,1,".strlen($pdr).")=$pdr ";}
  }  
  
  if (strlen($file)>0) {$QUERY.=" and indice_file=$file ";}
	if (strlen($matricola)>0) {$QUERY.=" and substring(matricola, -".strlen($matricola).")='$matricola' ";}
	if (strlen($codiceutente)>0) {$QUERY.=" and codice_utente=$codiceutente";}
	if (strlen($nominativo)>0) {$QUERY.=" and substring(utente,1,".strlen($nominativo).")='$nominativo' ";}
	if (strlen($flag)>0) {$QUERY.=" and flag='$flag' ";}
	if (strlen($esito)>0) {$QUERY.=" and (segn1='$esito' or segn2='$esito' or segn3='$esito') ";}
	if (strlen($acc)>0) {$QUERY.=" and acc='$acc' ";}
	if (strlen($controlli)>0) {$QUERY.=" and controlli='$controlli' ";}
	if ($note=="X") {$QUERY.=" and not (note is null or note='') ";}
	if (strlen($localita)>0) {$QUERY.=" and substring(localita,1,".strlen($localita).")='$localita' ";}
	if (strlen($comune)>0) {$QUERY.=" and substring(comune,1,".strlen($comune).")='$comune' ";}
	$filtroattivo=0;
	if (strlen($QUERY)>0){$filtroattivo=1;}
	
	$QUERY_EXTRA=$_SESSION["query_ente_sede"];
	if (strlen($file)>0) {$QUERY_EXTRA="";}
	
	Apri_DB("sede");
	
	
	switch($fase){
		
		case "filtra_tipi_file":
		
		 
		 if (isset($_POST['cLocalita'])){$cLocalita=CLEAN_VAR($_POST['cLocalita']);}
		 $_SESSION["filtro_show_cLocalita"]=$cLocalita;
		
		 $cLetture="0";
		 $cCartoline="0";
		 $cRipassi="0";
		 $cAutoletture="0";
		 
		 if (isset($_POST['cLetture'])){$cLetture=CLEAN_VAR($_POST['cLetture']);}
		 if (isset($_POST['cCartoline'])){$cCartoline=CLEAN_VAR($_POST['cCartoline']);}
		 if (isset($_POST['cRipassi'])){$cRipassi=CLEAN_VAR($_POST['cRipassi']);}
		 if (isset($_POST['cAutoletture'])){$cAutoletture=CLEAN_VAR($_POST['cAutoletture']);}
		 $_SESSION["filtro_show_tipi_file"]=$cLetture.$cCartoline.$cRipassi.$cAutoletture;
		 if (isset($_POST['filtro_show_nome_file'])){$filtro_show_nome_file=CLEAN_VAR($_POST['filtro_show_nome_file']);}
		 $_SESSION["filtro_show_nome_file"]=$filtro_show_nome_file;
		 $_SESSION["file_pagina"]=1;
		 break;
		
		case "filtra_cartoline":
		 if ($_SESSION["filtro_show_cartoline"]==1){$_SESSION["filtro_show_cartoline"]=0;} else{$_SESSION["filtro_show_cartoline"]=1;}
		 break;
		
		case "showfiles":
		 if ($_SESSION["mostra_archivio_giri"]==0){$_SESSION["mostra_archivio_giri"]=1;} else {$_SESSION["mostra_archivio_giri"]=0;}
		 break;
		 	
		
		case "eye": 
		  $_SESSION["vedi_file_checkati"]=$eye_value; 
		  $_SESSION["file_pagina"]=1;
		  break;
		
		case "spunta_file":
		 if ($checked==1){$checked=0;}else{$checked=1;}
		 $query="update caricofile set caricofile_checked=$checked where caricofile_id=$file_id";
		 $ris_fase=mysql_query($query)  or die("Query non valida: $query <br>" . mysql_error());
		 break;
		
		
   	case "filtra":
   	 
		 break;
		 
   	case "pagina_succ":
		 $_SESSION["pagina"] =$_SESSION["pagina"]+1;
		 if ($_SESSION["pagina"]>$_SESSION["tot_pagine"]){$_SESSION["pagina"]=$_SESSION["tot_pagine"];}
		 break;
		case "pagina_prec":
		 $_SESSION["pagina"] =$_SESSION["pagina"]-1;
		 if ($_SESSION["pagina"]<=0){$_SESSION["pagina"]=1;}
		 break; 
		case "pagina_prima":
		 $_SESSION["pagina"]=1;
		 break; 
		  
		case "pagina_ultima":
		 $_SESSION["pagina"] =$_SESSION["tot_pagine"];
		 break;  
		 
		case "file_pagina_succ":
		 //die("Pp: ".$_SESSION["file_pagina"]);
		 $_SESSION["file_pagina"]+=1;
		 if ($_SESSION["file_pagina"]>$_SESSION["file_tot_pagine"]){
		 	 $_SESSION["file_pagina"]=$_SESSION["file_tot_pagine"];
		 }
		 break;
		 
		case "file_pagina_prec":
		 $_SESSION["file_pagina"] =$_SESSION["file_pagina"]-1;
		 if ($_SESSION["file_pagina"]<=0){$_SESSION["file_pagina"]=1;}
		 break; 
		case "file_pagina_prima":
		 $_SESSION["file_pagina"] =1;
		 break;  
		case "file_pagina_ultima":
		 $_SESSION["file_pagina"] =$_SESSION["file_tot_pagine"];
		 break;   
		 
   }
	
	
	if (strlen($file)>0 || strlen($matricola)>0 || strlen($codiceutente)>0 || strlen($pdr)>0) {
		$QUERY_LETTURE = $QUERY; //.$_SESSION["query_ente_sede"];
		if (strlen($QUERY)>0){$QUERY_LETTURE="select * from letture where progressivo>$limite_inferiore_lavorazioni and stato in ('SPE') $QUERY_LETTURE $QUERY_EXTRA order by progressivo desc, sequenza asc limit ".($_SESSION["pagina"]-1)*$_SESSION["righe_per_pagina"].",".$_SESSION["righe_per_pagina"];}
		$RIS_LETTURE=mysql_query($QUERY_LETTURE);
  }		

	
	$CONDIZIONE_QUERY_FILE=$_SESSION["query_ente_file"]." and caricofile_progressivo>$limite_inferiore_lavorazioni  order by caricofile_id desc";
	$QUERY_FILE="select * from caricofile ".$CONDIZIONE_QUERY_FILE;
	//echo $QUERY_FILE;
	$ris_file=mysql_query($QUERY_FILE);
	
	$qCart="";
	if ($_SESSION["ut-ente"]==1){
		$qCart=" and (1=2 ";
		if(!($_SESSION["filtro_show_tipi_file"]=="0000" || $_SESSION["filtro_show_tipi_file"]=="1111")){
			if(substr($_SESSION["filtro_show_tipi_file"],0, 1)=="1"){$qCart.=" or (substring(b.caricofile_nome,8, 1)<>'A' and substring(b.caricofile_nome,1, 4)<>'CAR_' and substring(b.caricofile_nome,8, 1)<>'C') "; $Order=" order by substring(a.extra, 1, 6) desc, substring(b.caricofile_nome, -10) desc";}
			if(substr($_SESSION["filtro_show_tipi_file"],1, 1)=="1"){$qCart.=" or substring(b.caricofile_nome,8, 1)='C' "; $Order=" order by substring(b.caricofile_nome, 8, 3) desc";}
			if(substr($_SESSION["filtro_show_tipi_file"],2, 1)=="1"){$qCart.=" or instr('".$_SESSION["lista_lavorazioni_ripassi"]."',concat('|',b.caricofile_progressivo,'|'))>0 "; $Order=" order by substring(b.caricofile_nome, 8, 3) desc";}
			if(substr($_SESSION["filtro_show_tipi_file"],3, 1)=="1"){$qCart.=" or substring(b.caricofile_nome,8, 1)='A' "; $Order=" order by substring(b.caricofile_nome, 8, 3) desc";}
			$qCart.=") ";
			$len=strlen($_SESSION["filtro_show_nome_file"]);
			if(strlen($_SESSION["filtro_show_nome_file"])>0){$qCart.=" and substring(b.caricofile_nome, 1, $len)='".$_SESSION["filtro_show_nome_file"]."' ";}
			//die($qCart); 
	  }
	  //die($qCart);
	  while (strpos($qCart, "  ")!==false){
	  	$qCart=str_replace("  ", " ", $qCart);
	  }
	  
	  if ($qCart==" and (1=2 ) " || trim($qCart)=="and (1=2"){$qCart="";}
  }
  
  $qLocalitaFile="";
  if(isset($_SESSION["filtro_show_cLocalita"])){
  	if ($_SESSION["filtro_show_cLocalita"]!=""){
  	  $qLocalitaFile=" and substring(b.caricofile_nome, 5, 3)='".$_SESSION["filtro_show_cLocalita"]."' ";
  	}  
  }
  

	//$QUERY_FILE="select * from caricofile order by caricofile_id desc";
	$CONDIZIONE_QUERY_FILE_TOP="where b.caricofile_progressivo>$limite_inferiore_lavorazioni $q_giri_comune $q_giri_giro $q_giri_periodo $qCart $qLocalitaFile";
	
	if ($_SESSION["ut-ente"]==1){
		$query_ente=" and a.codice_ente in (1,7) ";
	} else {
	  $query_ente=" and a.codice_ente=".$_SESSION["ut-ente"]." ";
	}
	
	$FROM_FILE_TOP="from letture a inner join caricofile b on a.progressivo=b.caricofile_progressivo and a.indice_file=b.caricofile_id ".$query_ente;
	
	
	$qEye="";
  if ($_SESSION["vedi_file_checkati"]==0){$qEye=" and caricofile_checked=0 "; }
    
	$QUERY_FILE= "select 
				             b.caricofile_id,
				             b.caricofile_nome, 
				             b.caricofile_checked, 
				             substring(b.caricofile_nome, 8, 3) as giro,  
				             substring(a.extra, 1, 6) as periodo,  
				             b.caricofile_data, 
				             b.caricofile_data_in, 
				             b.caricofile_data_out, sum(1) as tot_letture,
				             group_concat(distinct(a.terminale) order by a.terminale asc) AS terminali,
				             sum(if(stato='LET', 1, 0)) as no_spe,
				             sum(if(stato='SPE', 1, 0)) as spe
	             $FROM_FILE_TOP
	             $CONDIZIONE_QUERY_FILE_TOP
	             $qEye
	             group by b.caricofile_id
	             $Order
	             limit ".($_SESSION["file_pagina"]-1)*$_SESSION["file_righe_per_pagina"].",".$_SESSION["file_righe_per_pagina"];
  //echo($QUERY_FILE);
	$ris_file_head=mysql_query($QUERY_FILE) or die("Query non valida: $QUERY_FILE <br>" . mysql_error());
	
	if (strlen($file)>0){
		$QUERY_LOCALITA="select localita from letture where $QUERY group by localita order by localita asc";
		//echo $QUERY_LOCALITA;
		$ris_loc=mysql_query($QUERY_LOCALITA);
		$QUERY_COMUNE="select comune from letture where $QUERY group by comune order by comune asc";
		$ris_comune=mysql_query($QUERY_COMUNE);
	}
	
	Apri_DB("system");
  $query="select * from lavorazione order by lavorazione_progressivo desc";
  $ris_lavorazione=mysql_query($query);
	
	

	Apri_DB("sede");
	
// __________________________________________________________________________________________________________________________________

$larghezza_2="100";
$larghezza_campi_filtri="150";
$larghezza_testi_filtri="146";
$larghezza_filtri_lunghi="380";
$larghezza_testi_filtri_lunghi="376";
$bordo="0";

$col1="#c0c4d0";
$col2="#c6cad5";

$style_check='style="width:200px;  color:#ffffff"';
$style_check_box='style="background-color:#ff0000;"';	 

echo'	
<body>
    <div id="contenitore">
     <div id="sito" >
      
      <div id="header"  >
       <TABLE WIDTH="100%" border="0">
	       <TR>
		      <TD ALIGN="RIGHT" VALIGN="TOP">
		       <SPAN CLASS="testogrigio">
		        '.$utilizzatore_id.' - '.$utilizzatore_alias.' ['.$utilizzatore_ruolo.' - '.$_SESSION["ruoli"][$utilizzatore_ruolo].'] 
		        [<a href="index.php?fase=logout" target="_self">logout</a>]
		       </SPAN>
		      </TD>
		     </TR> 	  
    	  </TABLE> 
 	    </div><!-- Header -->
 	    
	    <div id="filtri" >
	      <TABLE CELLPADDING="0" CELLSPACING="0" WIDTH="100%" border="0">
	       <TR>
	        <TD>
	         <TABLE>';
	         
	         if ($_SESSION["mostra_download_foto"]==1 && $utilizzatore_ruolo<=10){
		       		echo'
				      <TR>
			         <TD>
			          <TABLE CELLPADDING="2" CELLSPACING="0" BORDER="0">
			           <TR>
			            <TD VALIGN="BOTTOM"><TABLE BGCOLOR="#a9b0bd" CELLPADDING="2" width="140"><TR><TD class="giribt">DOWNLOAD FOTO</TD></TR></TABLE></TD>
			            <form id="spunta_'.$file_id.'" action="zippafoto.php" method="POST" target="_self">
									  <input type="hidden" name="fase" value="zippa_foto">
					            <TD ALIGN="CENTER" COLSPAN=2 CLASS="toptable" STYLE="border:solid 1 1px;"> <!------------- NOTE --------------->
					               
										     <SELECT ID="lavorazione" NAME="lavorazione" class="gea4" style="WIDTH:800PX;"> 
										       <OPTION VALUE=""></OPTION>';
										       
										       while ($rslav=mysql_fetch_array($ris_lavorazione)){echo'<OPTION VALUE="'.$rslav["lavorazione_progressivo"].'"'; echo'>'.$rslav["lavorazione_progressivo"]." - ".$rslav["lavorazione_nome"].'</OPTION>';}
										     
										     echo' 
										     </SELECT>  
										     <input class="bottonelungo" type="submit" name="" value="ZIPPA FOTO">
										    </TD>
									</form>
									<form id="spunta_'.$file_id.'" action="zippafoto.php" method="POST" target="_self">
									  <input type="hidden" name="fase" value="dir_zip">
					            <TD ALIGN="CENTER" COLSPAN=2 CLASS="toptable" STYLE="border:solid 1 1px;"> <!------------- NOTE --------------->
										     <input class="bottonelungo" type="submit" name="" value="CARTELLA ZIP">
										    </TD>
										   
									</form>
									</TR> 	  
			          </TABLE>
			         </TD>
			        </TR>';
					   }  
	         
	         echo'
	         
	         </TABLE>
	        </TD>
	       </TR>
	       <!--------------------------------------- TABELLA LOTTI ----------------------------------------------------------->
	       <TR>
	        <TD>
	         <TABLE cellpadding="0" cellspacing="0">
	         
	         
	            <TR>
		           <TD VALIGN="BOTTOM">
				             <TABLE BGCOLOR="#a9b0bd" CELLPADDING="2" width="200">
				              <TR>
				               <TD height="18">
			                   <A HREF="'.$pg.'?fase=showfiles" class="giribt">
			                    ARCHIVIO GIRI '; 
			                    if ($_SESSION["mostra_archivio_giri"]==1){echo"^";}else{echo"=";} 
			                    echo'
			                   </A>
				               </TD>
				              <TR>
				             </TABLE>
		            </TD>
		            <form id="filtra_cart" action="'.$pg.'" method="POST" target="_self">
								<input type="hidden" name="fase" value="filtra_tipi_file">
		            <TD VALIGN="BOTTOM" WIDTH="350PX"> 
					             <TABLE BGCOLOR="#a9b0bd" CELLPADDING="2" >
					               <TR>
							             <TD valign="center"><input '.$style_check_box.' type="checkbox" '; if(substr($_SESSION["filtro_show_tipi_file"], 0, 1)=="1"){echo "checked";} echo' name="cLetture" value="1"></TD><TD '.$style_check.' valign="center"><b>Letture</b></TD>
							             <TD valign="center"><input '.$style_check_box.' type="checkbox" '; if(substr($_SESSION["filtro_show_tipi_file"], 1, 1)=="1"){echo "checked";} echo' name="cCartoline" value="1"></TD><TD '.$style_check.' valign="center"><b>Cartoline</b></TD>
							             <TD valign="center"><input '.$style_check_box.' type="checkbox" '; if(substr($_SESSION["filtro_show_tipi_file"], 2, 1)=="1"){echo "checked";} echo' name="cRipassi" value="1"></TD><TD '.$style_check.' valign="center"><b>Ripassi</b></TD>
							             <TD valign="center"><input '.$style_check_box.' type="checkbox" '; if(substr($_SESSION["filtro_show_tipi_file"], 3, 1)=="1"){echo "checked";} echo' name="cAutoletture" value="1"></TD ><TD '.$style_check.' valign="center"><b>Autoletture</b></TD>
							             <TD WIDTH="15"></td>
							             <TD '.$style_check.' align="right"><b>File:</b></TD>
							             <TD><INPUT CLASS="gea4" TYPE="text" NAME="filtro_show_nome_file" VALUE="'.$filtro_show_nome_file.'" style="WIDTH:200px;"></TD>
							             
					               </TR>
					             </TABLE>
		            </TD>
		           <TD BGCOLOR="#a9b0bd" WIDTH="100%">
		          </TD>
		         </TR>  
		          
	         </TABLE>
	        </TD>
	       </TR>  
	       <TR>
	        <TD>
	         <TABLE border="0" width="100%">
	         
	            <TR>
		           <TD BGCOLOR="#a9b0bd" '.$style_check.' align="right"><b>Localit�:</b></TD>
		           <TD style="width:160px;" BGCOLOR="#a9b0bd">&nbsp;
		              <SELECT ID="cLocalita" NAME="cLocalita" class="gea4" style="WIDTH:'.$larghezza_campi_filtri.'PX;"> 
							        <OPTION VALUE="">tutte</OPTION>
							        <OPTION VALUE="120"'; if ($cLocalita=="120") echo' SELECTED'; echo'>Almese</OPTION>
							        <OPTION VALUE="120"'; if ($cLocalita=="029") echo' SELECTED'; echo'>Alpignano</OPTION>
							        <OPTION VALUE="049"'; if ($cLocalita=="049") echo' SELECTED'; echo'>Avigliana</OPTION>
							        <OPTION VALUE="037"'; if ($cLocalita=="037") echo' SELECTED'; echo'>Beinasco</OPTION>
							        <OPTION VALUE="098"'; if ($cLocalita=="098") echo' SELECTED'; echo'>Borgone Susa</OPTION>
							        <OPTION VALUE="038"'; if ($cLocalita=="038") echo' SELECTED'; echo'>Bruino</OPTION>
							        <OPTION VALUE="076"'; if ($cLocalita=="076") echo' SELECTED'; echo'>Bussoleno</OPTION>
							        <OPTION VALUE="030"'; if ($cLocalita=="030") echo' SELECTED'; echo'>Buttigliera</OPTION>
							        <OPTION VALUE="039"'; if ($cLocalita=="039") echo' SELECTED'; echo'>Candiolo</OPTION>
							        <OPTION VALUE="077"'; if ($cLocalita=="077") echo' SELECTED'; echo'>Caprie</OPTION>
							        <OPTION VALUE="055"'; if ($cLocalita=="029") echo' SELECTED'; echo'>Cascine Vica</OPTION>
							        <OPTION VALUE="055"'; if ($cLocalita=="055") echo' SELECTED'; echo'>Castagnole</OPTION>
							        <OPTION VALUE="091"'; if ($cLocalita=="091") echo' SELECTED'; echo'>Chiusa S. Michele</OPTION>
							        <OPTION VALUE="056"'; if ($cLocalita=="056") echo' SELECTED'; echo'>Coazze</OPTION>
											<OPTION VALUE="008"'; if ($cLocalita=="008") echo' SELECTED'; echo'>Collegno</OPTION>
											<OPTION VALUE="059"'; if ($cLocalita=="059") echo' SELECTED'; echo'>Condove</OPTION>
											<OPTION VALUE="040"'; if ($cLocalita=="040") echo' SELECTED'; echo'>Giaveno</OPTION>
											<OPTION VALUE="011"'; if ($cLocalita=="011") echo' SELECTED'; echo'>Grugliasco</OPTION>
											<OPTION VALUE="297"'; if ($cLocalita=="297") echo' SELECTED'; echo'>Mattie</OPTION>
											<OPTION VALUE="204"'; if ($cLocalita=="204") echo' SELECTED'; echo'>Meana di Susa</OPTION>
											<OPTION VALUE="264"'; if ($cLocalita=="264") echo' SELECTED'; echo'>None</OPTION>
											<OPTION VALUE="044"'; if ($cLocalita=="044") echo' SELECTED'; echo'>Piossasco</OPTION>
											<OPTION VALUE="064"'; if ($cLocalita=="064") echo' SELECTED'; echo'>Reano</OPTION>
											<OPTION VALUE="124"'; if ($cLocalita=="124") echo' SELECTED'; echo'>Riva presso Chieri</OPTION>
											<OPTION VALUE="045"'; if ($cLocalita=="045") echo' SELECTED'; echo'>Rivalta</OPTION>
											<OPTION VALUE="029"'; if ($cLocalita=="029") echo' SELECTED'; echo'>Rivoli</OPTION>
											<OPTION VALUE="031"'; if ($cLocalita=="031") echo' SELECTED'; echo'>Rosta</OPTION>
											<OPTION VALUE="113"'; if ($cLocalita=="113") echo' SELECTED'; echo'>Leini</OPTION>
											<OPTION VALUE="116"'; if ($cLocalita=="116") echo' SELECTED'; echo'>Rubiana</OPTION>
											<OPTION VALUE="070"'; if ($cLocalita=="249") echo' SELECTED'; echo'>Saluggia</OPTION>
											<OPTION VALUE="070"'; if ($cLocalita=="070") echo' SELECTED'; echo'>Sant\'Ambrogio</OPTION>
											<OPTION VALUE="222"'; if ($cLocalita=="222") echo' SELECTED'; echo'>Sant\'Antonino</OPTION>
											<OPTION VALUE="125"'; if ($cLocalita=="125") echo' SELECTED'; echo'>San Didero</OPTION>
											<OPTION VALUE="298"'; if ($cLocalita=="298") echo' SELECTED'; echo'>San Giorio di Susa</OPTION>
											<OPTION VALUE="285"'; if ($cLocalita=="285") echo' SELECTED'; echo'>San Secondo</OPTION>
											<OPTION VALUE="046"'; if ($cLocalita=="046") echo' SELECTED'; echo'>Sangano</OPTION>
											<OPTION VALUE="082"'; if ($cLocalita=="082") echo' SELECTED'; echo'>Susa</OPTION>
											<OPTION VALUE="001"'; if ($cLocalita=="001") echo' SELECTED'; echo'>TORINO</OPTION>
											<OPTION VALUE="289"'; if ($cLocalita=="289") echo' SELECTED'; echo'>Torre Pellice</OPTION>
											<OPTION VALUE="057"'; if ($cLocalita=="057") echo' SELECTED'; echo'>Trana</OPTION>
											<OPTION VALUE="096"'; if ($cLocalita=="096") echo' SELECTED'; echo'>Vaie</OPTION>
											<OPTION VALUE="065"'; if ($cLocalita=="065") echo' SELECTED'; echo'>Valgioie</OPTION>
											<OPTION VALUE="292"'; if ($cLocalita=="292") echo' SELECTED'; echo'>Vigone</OPTION>
											<OPTION VALUE="294"'; if ($cLocalita=="294") echo' SELECTED'; echo'>Villar Pellice</OPTION>
											<OPTION VALUE="052"'; if ($cLocalita=="052") echo' SELECTED'; echo'>Villarbasse</OPTION>
											<OPTION VALUE="072"'; if ($cLocalita=="072") echo' SELECTED'; echo'>Villardora</OPTION>
											<OPTION VALUE="296"'; if ($cLocalita=="296") echo' SELECTED'; echo'>Virle</OPTION>
							     </SELECT> 
		             </TD>
			           <TD BGCOLOR="#a9b0bd" STYLE="width:100px;">&nbsp;<input class="bottone" type="submit" value="Filtra"></TD>
		             <TD>
		             ';
		           
		           if ($_SESSION["mostra_archivio_giri"]==1){PaginazioneFile();}
		           
		           echo'</TD>';
		           
		           if ($utilizzatore_ruolo!=20){
		           	echo'
		             <TD VALIGN="BOTTOM" WIDTH="50"><TABLE CELLSPACING="0" CELLPADDING="0"><TR><TD><a href="maps.php" target="_self"><IMG SRC="images/btmaps.jpg" border="0"></a></TD></TR></TABLE></TD>
		            ';
		           }
		           
		          echo'   
		          </TR> 
	         
	         </TABLE>
	        </TD>
	       </TR>
	      </form>';
	      
		       if ($_SESSION["mostra_archivio_giri"]==1){
		       	
		       	echo'
				       
				       <TR>
				        <TD> 
				        
				         <TABLE width="100%" CELLPADDING="2" style="background-image:url(\'images/sfondo_sfumato.jpg\'); background-color:#a9b0bd; background-repeat:repeat-x;">
				         <TR>
				          <TD>';
				          
				           echo'
					         <table align="center" width="100%" cellpadding="0" cellspacing="0" >
					          <tr>'; 
					         
					          $classe="toptabletop"; //toptableletturegiri";
					          
					          echo'<td align="center" valign="center" class="'.$classe.'">';
					          
					           if ($utilizzatore_ruolo!=20){
					            if ($_SESSION["vedi_file_checkati"]==0){
					            	echo'<a href="'.$pg.'?fase=eye&eye_value=1" target="_self"><img src="images/eye_off.jpg" border="0"></a>';
					            } else {
					            	echo'<a href="'.$pg.'?fase=eye&eye_value=0" target="_self"><img src="images/eye_on.jpg" border="0"></a>';
					          	}
					           }	
					            
					          echo'</td>';
					          
					          if ($utilizzatore_ruolo!=20){
					            fill_cella(1, 'Zip', 'center', $classe);
					          }  
					          
					          fill_cella(1, 'File', 'center', $classe);
					          fill_cella(1, 'Comune', 'center', $classe);
					          fill_cella(1, 'Giro', 'center', $classe);
					          fill_cella(1, 'Periodo', 'center', $classe);
					          fill_cella(1, 'Data In', 'center', $classe);
					          fill_cella(1, 'Data Out', 'center', $classe);
					          fill_cella(1, 'Tot Lett', 'center', $classe);
					          fill_cella(1, 'In Lett', 'center', $classe);
					          fill_cella(1, 'Ricons.', 'center', $classe);
					          fill_cella(1, '%', 'center', $classe);
					          fill_cella(1, 'Letturista', 'center', $classe);
					          
					          
					          
					          echo'</tr><tr>';
				          
				           if (mysql_num_rows($ris_file_head)>0){
				          
							          //$QUERY_FILE="select a.localita, substring(b.caricofile_nome, 8, 3) as giro,  substring(a.extra, 1, 6) as periodo,  b.caricofile_data_in, b.caricofile_data_out, sum(1) as tot_letture, sum(if(not(stato='SPE'), 1, 0)) as no_spe, sum(if(stato='SPE', 1, 0)) as spe from letture a inner join caricofile b on a.indice_file=b.caricofile_id group by b.caricofile_id, a.localita order by b.caricofile_id, a.localita desc";
							          
							          $indice=-1;
							          $mem_caricofile_nome="";
											  while ($riga=mysql_fetch_array($ris_file_head)){
											  	
											   $checked= $riga["caricofile_checked"];
											   $caricofile_nome=$riga["caricofile_nome"];
											   
											   if (!($_SESSION["vedi_file_checkati"]==0 && $checked==1)&&strtoupper(substr($caricofile_nome, -3))=="INP"){	
											   	
											   	 $mem_caricofile_nome=$caricofile_nome;
											   	 
											   	 
												   $indice=$indice+1;	
												   if ($indice % 2){$classe="cellagrigliagiri";} else {$classe="cellagrigliabiancagiri";}
												   
												   
												   $file_id=$riga['caricofile_id'];
												   
												   echo '
												    <form id="spunta_'.$file_id.'" action="'.$_SERVER['PHP_SELF'].'" method="POST" target="_self">
													    <td class="'.$classe.'" align="right" style="width:25px; height:5px;">
													     <input type="hidden" name="fase" value="spunta_file">
													     <input type="hidden" name="file_id" value="'.$file_id.'">
													     <input type="hidden" name="checked" value="'.$checked.'">
													     <input type="checkbox" name="" value="" ';
						                   if ($checked==1){ echo ' checked ';}
						                   echo '
						                    onclick="document.getElementById(\'spunta_'.$file_id.'\').submit(); return false">
													    </td>
													  </form>  '; 
												    
												   $tot_lette= $riga["tot_letture"];
												   $no_spe= $riga["no_spe"];
												   $spe=$riga["spe"];
												   $perc=floor((($no_spe+$spe)/$tot_lette)*100);
												   $periodo=$riga["periodo"];
												   if (substr($caricofile_nome, 7, 1)=="A"){$periodo="20".substr($caricofile_nome, 0, 4);}
												   
												   $periodo_filtro=$periodo;
												   if (strlen($periodo)==6){$periodo= substr($periodo, -2)."/".substr($periodo, 0, 4);}
												   $giro=$riga["giro"];
												   
												   
												   if ($utilizzatore_ruolo!=20){
													   echo'
													    
													     <!-- download file dati -->
													     <form>
														     <input type="hidden" name="fase" value="file_dati">
														     <td class="'.$classe.'" align="center">';
														     
														      if ($spe>0){
														      	echo'
																      <a href="zip.php?fase=dwnld_all&file_id='.$file_id.'" target="_self">
																       <img src="images/zip.png" border="0">
																      </a> 
																    ';
																  } else {
																  	echo'<img src="images/zipoff.png" border="0">';
																  }	  
																    
																 echo'     
														     </td>
													     </form>
													     
													     <!-- download file foto -->
													     <!--<form>
														     <input type="hidden" name="fase" value="file_dati">
														     <td class="'.$classe.'" align="center">
														      <a href="zip.php?fase=dwnld_foto&file_id='.$file_id.'" target="_self">
														       <img src="images/img_dwnld_foto.png" border="0"> 
														      </a> 
														     </td>
													     </form>-->
													   ';
													 }  
												   
												   
												   
												   switch (substr($caricofile_nome, 4, 3)){
												   	  
									          	case "001": $file_comune="TORINO"; break;
															case "008": $file_comune="Collegno"; break;
															case "011": $file_comune="Grugliasco"; break;
															case "029": $file_comune="Rivoli"; break;
															case "030": $file_comune="Buttigliera"; break;
															case "031": $file_comune="Rosta"; break;
															case "037": $file_comune="Beinasco"; break;
															case "038": $file_comune="Bruino"; break;
															case "039": $file_comune="Candiolo"; break;
															case "040": $file_comune="Giaveno"; break;
															case "044": $file_comune="Piossasco"; break;
															case "045": $file_comune="Rivalta"; break;
															case "046": $file_comune="Sangano"; break;
															case "049": $file_comune="Avigliana"; break;
															case "052": $file_comune="Villarbasse"; break;
															case "055": $file_comune="Castagnole"; break;
															case "056": $file_comune="Coazze"; break;
															case "057": $file_comune="Trana"; break;
															case "059": $file_comune="Condove"; break;
															case "064": $file_comune="Reano"; break;
															case "065": $file_comune="Valgioie"; break;
															case "070": $file_comune="Sant'Ambrogio"; break;
															case "072": $file_comune="Villardora"; break;
															case "076": $file_comune="Bussoleno"; break;
															case "077": $file_comune="Caprie"; break;
															case "082": $file_comune="Susa"; break;
															case "091": $file_comune="Chiusa S. Michele"; break;
															case "096": $file_comune="Vaie"; break;
															case "098": $file_comune="Borgone Susa"; break;
															case "113": $file_comune="Leini"; break;
															case "116": $file_comune="Rubiana"; break;
															case "120": $file_comune="Almese"; break;
															case "124": $file_comune="Riva presso Ghieri"; break;
															case "125": $file_comune="San Didero"; break;
															case "204": $file_comune="Meana di Susa"; break;
															case "222": $file_comune="Sant'Antonino"; break;
															case "249": $file_comune="Saluggia"; break;
															case "264": $file_comune="None"; break;
															case "285": $file_comune="San Secondo"; break;
															case "289": $file_comune="Torre Pellice"; break;
															case "292": $file_comune="Vigone"; break;
															case "294": $file_comune="Villar Pellice"; break;
															case "296": $file_comune="Virle"; break;
															case "297": $file_comune="Mattie"; break;
															case "298": $file_comune="San Giorio di Susa"; break;
															
														}	
														
														if (substr($caricofile_nome, 4, 3)=="001"){
															switch (substr($caricofile_nome, 7, 3)){
																case "480":
																case "481":
																case "482":
																case "485":
																case "490":
																case "492":
																case "495":
																case "500":
																case "502":
																case "504":
																case "506":
																case "508":
																case "510":
																case "512":
																case "514":
																case "516":
																case "518":
																case "520":
																case "525":
																case "527":
																case "528":
																case "530":
																case "531":
																case "533":
																case "534":
																case "535":
																case "537":
																case "538":
																case "540":
																case "542":
																case "543":
																case "547":
																case "548": $file_comune="TORINO GRUPPO 1"; break;
																case "550":
																case "551":
																case "553":
																case "555":
																case "557":
																case "560":
																case "562":
																case "565":
																case "567":
																case "569":
																case "570":
																case "571":
																case "573":
																case "575":
																case "576":
																case "579":
																case "580":
																case "582":
																case "583":
																case "586":
																case "587":
																case "588":
																case "591":
																case "595":
																case "597": $file_comune="TORINO GRUPPO 1A"; break;
																case "600":
																case "601":
																case "603":
																case "604":
																case "606":
																case "607":
																case "610":
																case "611":
																case "613":
																case "614":
																case "615":
																case "617":
																case "618":
																case "619":
																case "621":
																case "623":
																case "625":
																case "630":
																case "631":
																case "634":
																case "635":
																case "638":
																case "640":
																case "644":
																case "645": $file_comune="TORINO GRUPPO 2"; break;
																case "650":
																case "652":
																case "653":
																case "654":
																case "656":
																case "658":
																case "659":
																case "660":
																case "661":
																case "662":
																case "665":
																case "667":
																case "668":
																case "670":
																case "671":
																case "672":
																case "673":
																case "675":
																case "677":
																case "678":
																case "680":
																case "682":
																case "683":
																case "690":
																case "691":
																case "692": $file_comune="TORINO GRUPPO 2A"; break;
																case "700":
																case "702":
																case "705":
																case "707":
																case "710":
																case "712":
																case "714":
																case "715":
																case "720":
																case "730":
																case "750":
																case "751":
																case "753":
																case "755":
																case "757":
																case "760":
																case "763":
																case "765":
																case "767":
																case "770":
																case "772":
																case "775":
																case "780":
																case "782":
																case "785":
																case "788": $file_comune="TORINO GRUPPO 3"; break;
																case "800":
																case "802":
																case "805":
																case "806":
																case "807":
																case "808":
																case "810":
																case "811":
																case "812":
																case "815":
																case "817":
																case "818":
																case "819":
																case "820":
																case "822":
																case "823":
																case "825":
																case "826":
																case "828":
																case "830":
																case "831":
																case "835":
																case "836":
																case "840":
																case "845": $file_comune="TORINO RUPPO 4"; break;
																case "850":
																case "851":
																case "854":
																case "855":
																case "857":
																case "858":
																case "860":
																case "862":
																case "863":
																case "865":
																case "867":
																case "868":
																case "870":
																case "871":
																case "872":
																case "874":
																case "875":
																case "876":
																case "880":
																case "883":
																case "885":
																case "887":
																case "889":
																case "890":
																case "895": $file_comune="TORINO GRUPPO 4A"; break;
																case "900":
																case "902":
																case "905":
																case "907":
																case "908":
																case "910":
																case "915":
																case "920":
																case "925":
																case "928":
																case "930":
																case "935":
																case "942":
																case "945":
																case "950":
																case "955":
																case "960":
																case "956":
																case "970":
																case "975":
																case "980":
																case "985":
																case "986": $file_comune="TORINO GRUPPO 5"; break;
																case "991":
																case "992":
																case "993":
																case "994":
																case "995":
																case "996":
																case "997":
																case "998":
																case "999": $file_comune="TORINO SPECIALI"; break;
															}	
														} // Torino	
														
														if (substr($caricofile_nome, 4, 3)=="029"){
															switch (substr($caricofile_nome, 7, 3)){
																case "001":
																case "002":
																case "003":
																case "004":
																case "006":
																case "018":
																case "019":
																case "021":
																case "022": $file_comune="RIVOLI 1"; break;
																case "007":
																case "008":
																case "009":
																case "011":
																case "012":
																case "013":
																case "014":
																case "016": $file_comune="RIVOLI 2"; break;
															}	
														} // Rivoli
														
														
												    
												   fill_cella(1, $caricofile_nome, 'left', $classe);
												   
												   $c="giri";
												   if (strlen($q_giri_comune)>0){$c="girisel";}
												   echo'
												    <td class="'.$classe.'" align="left">
												     <a href="'.$pg.'?giri_comune='.$file_comune.'&giri_file='.$caricofile_nome.'" target="_self" class="'.$c.'">
												      '.$file_comune.'
												     </a> 
												    </td>
												   ';
												   
												   $c="giri";
												   if (strlen($q_giri_giro)>0){$c="girisel";}
												   echo'
												    <td class="'.$classe.'" align="center">
												     <a href="'.$pg.'?giri_giro='.$giro.'" target="_self" class="'.$c.'">
												      '.$giro.'
												     </a> 
												    </td>
												   ';
												   
												   $c="giri";
												   if (strlen($q_giri_periodo)>0){$c="girisel";}
												   echo'
												    <td class="'.$classe.'" align="center">
												     <a href="'.$pg.'?giri_periodo='.$periodo_filtro.'" target="_self" class="'.$c.'">
												      '.$periodo.'
												     </a> 
												    </td>
												   ';
													 
												   $terminali=$riga["terminali"];
												   
												   
												   
												   //if (strpos($caricofile_nome, "CAR_")===false){
												   //	 fill_cella(2, 'caricofile_data_in','center', $classe);
												   //	 fill_cella(2, 'caricofile_data_out','center', $classe);
												   //} else {
												   //	 $data_out=$riga["caricofile_data_out"];
												   //	 $data_out= mktime (0,0,0,substr($data_out, 3, 2),substr($data_out, 0, 2)-2,substr($data_out, 6, 4));
                           //  $data_out=date('d/m/Y',$data_out);
												   //	 fill_cella(1, $data_out,'center', $classe);
												   //  fill_cella(2, 'caricofile_data','center', $classe);
												   //} 
												   
												   switch (substr($caricofile_nome, 7, 1)){
  												   case "C":
  												   case "A":
  												      $data_out=$riga["caricofile_data_out"];
														   	$data_out= mktime (0,0,0,substr($data_out, 3, 2),substr($data_out, 0, 2)-2,substr($data_out, 6, 4));
			                          $data_out=date('d/m/Y',$data_out);
														   	fill_cella(1, $data_out,'center', $classe);
														    fill_cella(2, 'caricofile_data','center', $classe);
  												   	  break;	  
  												    default:
  												      fill_cella(2, 'caricofile_data_in','center', $classe);
												   	    fill_cella(2, 'caricofile_data_out','center', $classe);
  												   	  break;	  
												   }
												   
												    
												   fill_cella(1, $tot_lette, 'left', $classe);
												   fill_cella(1, $no_spe,'left', $classe);
												   fill_cella(1, $spe,'left', $classe);
												   fill_cella(1, $perc." %",'center', $classe);
												   fill_cella(1, $terminali,'center', $classe);
												   
												   
												   
												   echo "</tr></tr><tr>";
												   
												  } // if eye 
											   
											   } // While
											   
							          
							          
							          
							         
							      }
							      
							      echo' 
					          </tr>
					         </table>';
							      
							    echo'     
					        </TD>
					       </TR>
					       </TABLE> <!-- Rettangolo restrittivo tabella file  -->
					       
				        </TD>
				       </TR>
				       <!--------------------------------------- FINE TABELLA LOTTI ----------------------------------------------------------->
				       
				      ';
				     } // Visualizzazione Lotti 
		       
		       echo' 
		       <TR><TD HEIGHT="25"></TD></TR>
		       <TR>
		        <TD>
		         <TABLE BGCOLOR="#a9b0bd" CELLPADDING="2" width="140"><TR><TD><FONT COLOR="#FFFFFF"><B>FILTRI</TD></TR></TABLE>
		        <TD>
		       </TR>
		       	       
		       
		       
		       <!--------------------------------------- PRIMA RIGA FILTRI ----------------------------------------------------------->
						<TR>
						 <TD>
						  <TABLE width="100%" CELLPADDING="10" CELLSPACING="0" style="background-image:url(\'images/sfondo_sfumato.jpg\'); background-color:#d1d4dc; background-repeat:repeat-x;">
			         <TR>
			          <TD>
			          <table cellpadding="0" align="center"><tr><td>
							  <form name="filtra" action="'.$_SERVER['PHP_SELF'].'" method="POST" target="_self">
							  <input type="hidden" name="fase" value="filtra">
							  <TABLE CELLPADDING="1PX" CELLSPACING="1PX" BORDER="'.$bordo.'" WIDTH="100%" ALIGN="CENTER">
							    <TR>
							     <TD ALIGN="CENTER" CLASS="'; if(strlen($pdr)>0){echo'toptablefiltrata';}else{echo'toptable';}echo'" WIDTH="'.$larghezza_2.'">PDR / POD / ULM</TD>
							     <TD ALIGN="CENTER" CLASS="'; if(strlen($matricola)>0){echo'toptablefiltrata';}else{echo'toptable';}echo'" WIDTH="'.$larghezza_campi_filtri.'">Matricola</TD>
							     <TD ALIGN="CENTER" CLASS="'; if(strlen($codiceutente)>0){echo'toptablefiltrata';}else{echo'toptable';}echo'" WIDTH="'.$larghezza_campi_filtri.'">Cod. Ut.</TD>
							     <TD ALIGN="CENTER" CLASS="'; if(strlen($nominativo)>0){echo'toptablefiltrata';}else{echo'toptable';}echo'" WIDTH="'.$larghezza_filtri_lunghi.'">Nominativo</TD>
							    </TR> 
							    <TR>
							     <TD ALIGN="CENTER" CLASS="toptable" WIDTH="'.$larghezza_2.'"> <!------------- PDR --------------->
							      <INPUT CLASS="gea4" TYPE="text" NAME="pdr" VALUE="'.$pdr.'" style="WIDTH:'.$larghezza_testi_filtri.'PX;">
							     </TD> 
							     <TD ALIGN="CENTER" CLASS="toptable" WIDTH="'.$larghezza_campi_filtri.'"> <!------------- MATRICOLA --------------->
							      <INPUT CLASS="gea4" TYPE="text" NAME="matricola" VALUE="'.$matricola.'" style="WIDTH:'.$larghezza_testi_filtri.'PX;">
							     </TD> 
							     <TD ALIGN="CENTER" WIDTH="30PX" CLASS="toptable"> <!------------- COD UTENTE --------------->
							      <INPUT CLASS="gea4" TYPE="text" NAME="codiceutente" VALUE="'.$codiceutente.'" style="WIDTH:'.$larghezza_testi_filtri.'PX;">
							     </TD> 
							     <TD ALIGN="CENTER" WIDTH="30PX" CLASS="toptable"> <!------------- NOMINATIVO --------------->
							      <INPUT CLASS="gea4" TYPE="text" NAME="nominativo" VALUE="'.$nominativo.'" style="WIDTH:'.$larghezza_testi_filtri_lunghi.'PX;">
							     </TD> 
							  </TABLE>
							 </TD>
							</TR> 
							<!--------------------------------------- FINE PRIMA RIGA FILTRI ----------------------------------------------------------->
							
							
							<!--------------------------------------- SECONDA RIGA FILTRI ----------------------------------------------------------->
							<TR>
							 <TD>
							  <TABLE CELLPADDING="1PX" CELLSPACING="1PX" WIDTH="100%" HEIGHT="35" >
							   <TR>
							    <TD ALIGN="CENTER" CLASS="'; if(strlen($note)>0){echo'toptablefiltrata';}else{echo'toptable';}echo'" WIDTH="'.$larghezza_2.'" STYLE="border:solid '.$col1.' 1px;">Note</TD>
								  <TD ALIGN="CENTER" CLASS="'; if(strlen($esito)>0){echo'toptablefiltrata';}else{echo'toptable';}echo'" WIDTH="'.$larghezza_filtri_lunghi.'" STYLE="border:solid '.$col1.' 1px;">Esito</TD>
							    <TD ALIGN="CENTER" CLASS="'; if(strlen($localita)>0){echo'toptablefiltrata';}else{echo'toptable';}echo'" WIDTH="'.$larghezza_campi_filtri.'" STYLE="border:solid '.$col1.' 1px;">Localit�</TD>
							    <TD ALIGN="CENTER" CLASS="'; if(strlen($localita)>0){echo'toptablefiltrata';}else{echo'toptable';}echo'" WIDTH="'.$larghezza_campi_filtri.'" STYLE="border:solid '.$col1.' 1px;">Comune</TD>
							    <TD ALIGN="CENTER" CLASS="'; if(strlen($controlli)>0){echo'toptablefiltrata';}else{echo'toptable';}echo'" WIDTH="'.$larghezza_campi_filtri.'" STYLE="border:solid '.$col1.' 1px;">Controllate</TD>
							   </TR> 
							   <TR>
							    <TD ALIGN="CENTER" CLASS="toptable" WIDTH="'.$larghezza_2.'" STYLE="border:solid '.$col1.' 1px;"> <!------------- NOTE --------------->
							     <SELECT ID="note" NAME="note" class="gea4" style="WIDTH:'.$larghezza_campi_filtri.'PX;"> 
							       <OPTION VALUE="">tutte</OPTION>
							       <OPTION VALUE="X"'; if ($note=="X") echo' SELECTED'; echo'>Solo Note</OPTION>
							     </SELECT>  
							    </TD>
							    <TD ALIGN="CENTER" CLASS="toptable" WIDTH="'.$larghezza_filtri_lunghi.'" STYLE="border:solid '.$col1.' 1px;"> <!------------- ESITO --------------->
							      <SELECT ID="esito" NAME="esito" class="gea4" style="WIDTH:'.$larghezza_filtri_lunghi.'PX;">
							       <OPTION VALUE="">tutti</OPTION>';
							       
							        Apri_DB("system");
							       
							        $QUERY_ESITI="select * from segnalazioni where ente_id=".$_SESSION["ut-ente"]." order by id ";
											$ris_esito=mysql_query($QUERY_ESITI);
							        while($riga=mysql_fetch_array($ris_esito)){
							        	$buff=$riga['codice'];
							        	if (strlen(trim($buff))>0){
							        	  echo'<OPTION VALUE="'.$buff.'" ';
							            if ($buff==$esito) echo' SELECTED';
							            echo'>'.$buff." - ".$riga['descrizione'].'</option>';      
							          }  
							        }
							        
							        Apri_DB("sede");
							        
							      echo' 
						        </SELECT>  
							    </TD>
							     
							    <TD ALIGN="CENTER" CLASS="toptable" WIDTH="'.$larghezza_campi_filtri.'" STYLE="border:solid '.$col1.' 1px;"> <!------------- LOCALITA --------------->
							      <SELECT ID="localita" NAME="localita" class="gea4" style="WIDTH:'.$larghezza_campi_filtri.'PX;"> 
							        <OPTION VALUE="">tutte</OPTION>';
							        
							        if (strlen($file)>0 && $ris_loc){
								        while($riga=mysql_fetch_array($ris_loc)){
								        	$buff=$riga['localita'];
								        	if (strlen(trim($buff))==0){$buff=$riga['comune'];}
								        	if (strlen(trim($buff))>0){
								        	  echo'<OPTION VALUE="'.$buff.'" ';
								            if ($buff==$localita) echo' SELECTED';
								            echo'>'.$buff.'</option>';      
								          }  
								        }
								      }
								        
						        echo'
						        </SELECT> 
							    </TD>
							    <TD ALIGN="CENTER" CLASS="toptable" WIDTH="'.$larghezza_campi_filtri.'" STYLE="border:solid '.$col1.' 1px;"> <!------------- COMUNE --------------->
							      <SELECT ID="comune" NAME="comune" class="gea4" style="WIDTH:'.$larghezza_campi_filtri.'PX;"> 
							        <OPTION VALUE="">tutti</OPTION>';
							        
							        if (strlen($file)>0 && $ris_comune){
								        while($riga=mysql_fetch_array($ris_comune)){
								        	$buff=$riga['comune'];
								        	if (strlen(trim($buff))>0){
								        	  echo'<OPTION VALUE="'.$buff.'" ';
								            if ($buff==$comune) echo' SELECTED';
								            echo'>'.$buff.'</option>';      
								          }  
								        }
								       } 
						        echo'
						        </SELECT> 
							    </TD>
						      <TD ALIGN="CENTER" CLASS="toptable" WIDTH="'.$larghezza_campi_filtri.'" STYLE="border:solid '.$col1.' 1px;"> <!------------- CONTROLLATE --------------->
							      <SELECT ID="controlli" NAME="controlli" class="gea4" style="WIDTH:'.$larghezza_campi_filtri.'PX;"> 
							        <OPTION VALUE=""'; if ($controlli!="0" && $controlli!="1") echo' SELECTED'; echo'>tutte</OPTION>
							        <OPTION VALUE="1"'; if ($controlli=="1") echo' SELECTED'; echo'>Controllate</OPTION>
							        <OPTION VALUE="0"'; if ($controlli=="0") echo' SELECTED'; echo'>NON Controllate</OPTION>
							      </SELECT>  
							    </TD> 
					        </TR>
					       </TABLE> 
					      </TD>
					     </TR>
					     <!--------------------------------------- FINE SECONDA RIGA FILTRI -----------------------------------------------------------> 
					     
					     
					     <!--------------------------------------- TERZA RIGA FILTRI -----------------------------------------------------------> 
					     <TR>
					      <TD>
						      <TABLE CELLPADDING="1PX" CELLSPACING="1PX" BORDER="'.$bordo.'" WIDTH="100%" ALIGN="CENTER">
								    <TR>
								     <TD ALIGN="CENTER" CLASS="'; if(strlen($flag)>0){echo'toptablefiltrata';}else{echo'toptable';}echo'" WIDTH="'.$larghezza_campi_filtri.'" STYLE="border:solid '.$col2.' 1px;">Consumi</TD>
									   <TD ALIGN="CENTER" CLASS="'; if(strlen($acc)>0){echo'toptablefiltrata';}else{echo'toptable';}echo'" WIDTH="'.$larghezza_campi_filtri.'" STYLE="border:solid '.$col2.' 1px;">Acc</TD>
									   <TD ALIGN="CENTER" CLASS="'; if(strlen($file)>0){echo'toptablefiltrata';}else{echo'toptable';}echo'" WIDTH="'.$larghezza_filtri_lunghi.'" STYLE="border:solid '.$col2.' 1px;">File</TD>
								    </TR> 
								    <TR>
								     <TD ALIGN="CENTER" CLASS="toptable" WIDTH="'.$larghezza_campi_filtri.'" STYLE="border:solid '.$col2.' 1px;"> <!------------- CONSUMI --------------->
								      <SELECT ID="flag" NAME="flag" class="gea4" style="WIDTH:'.$larghezza_campi_filtri.'PX;"> 
								        <OPTION VALUE="">tutti</OPTION>
								        <OPTION VALUE="G"'; if ($flag=="G") echo' SELECTED'; echo'>G-Giricontatore</OPTION>
								        <OPTION VALUE="I"'; if ($flag=="I") echo' SELECTED'; echo'>I-Negativi</OPTION>
								        <OPTION VALUE="N"'; if ($flag=="N") echo' SELECTED'; echo'>N-Nulli</OPTION>
								        <OPTION VALUE="B"'; if ($flag=="B") echo' SELECTED'; echo'>B-Bassi</OPTION>
								        <OPTION VALUE="L"'; if ($flag=="L") echo' SELECTED'; echo'>L-Normali</OPTION>
								        <OPTION VALUE="A"'; if ($flag=="A") echo' SELECTED'; echo'>A-Eccessivi</OPTION>
								      </SELECT>  
								     </TD> 
								     
								     <TD ALIGN="CENTER" CLASS="toptable" WIDTH="'.$larghezza_campi_filtri.'" STYLE="border:solid '.$col2.' 1px;"> <!------------- ACCESSIBILITA --------------->
								      <SELECT ID="acc" NAME="acc" class="gea4" style="WIDTH:'.$larghezza_campi_filtri.'PX;"> 
								        <OPTION VALUE=""'; if ($acc!="A" && $acc!="N" && $acc!="P" && $acc!="X") echo' SELECTED'; echo'>tutte</OPTION>
								        <OPTION VALUE="A"'; if ($acc=="A") echo' SELECTED'; echo'>A-Accessibili</OPTION>
								        <OPTION VALUE="P"'; if ($acc=="P") echo' SELECTED'; echo'>P-Parz. Acc.</OPTION>
								        <OPTION VALUE="N"'; if ($acc=="N") echo' SELECTED'; echo'>N-NON Accessibili</OPTION>
								        <OPTION VALUE="X"'; if ($acc=="X") echo' SELECTED'; echo'>&nbsp;&nbsp;-Non Censiti</OPTION>
								      </SELECT>  
								     </TD> 
								     <TD ALIGN="CENTER" WIDTH="260PX" CLASS="toptable"  STYLE="border:solid '.$col2.' 1px; width:'.$larghezza_filtri_lunghi.'px;"> <!------------- FILE --------------->
								      <SELECT ID="file" NAME="file" class="gea4" style="width:'.$larghezza_filtri_lunghi.'px;"> 
								        <OPTION VALUE="">tutti</OPTION>';
								        while($riga=mysql_fetch_array($ris_file)){
								        	$buff=$riga['caricofile_id'];
								        	if (strlen(trim($buff))==0){$buff=$riga['caricofile_id'];}
								        	if (strlen(trim($buff))>0){
								        	  echo'<OPTION VALUE="'.$buff.'" ';
								            if ($buff==$file){
								            	 echo' SELECTED';
								            	 $NomeFile=$riga['caricofile_nome'];
								            $impianto=substr($NomeFile, 4, 3);
								            $giro=substr($NomeFile, 7, 3);
								            }	 
								            
								            echo'>'.substr($riga['caricofile_data'], 3, 7)." - ".$riga['caricofile_nome'].'</option>';      
								          }  
								        }
					             echo'
					            </SELECT> 
					           </TD>
								    </TR>
								   </TABLE> 
								</TD>   
					     </TR>
					     <!--------------------------------------- FINE TERZA RIGA FILTRI -----------------------------------------------------------> 
					     
					     
					     
					     <!--------------------------------------- QUARTA RIGA FILTRI -----------------------------------------------------------> 
					     <TR>
						     <TD ALIGN="CENTER">
						      <TABLE>
						       <TR>
						        <TD CLASS="toptable"><input type="submit" class="'; if ($filtroattivo==1) {echo"bottonefiltrato";} else {echo"bottone";} echo '" value="Filtra"></TD></form> 
								    <TD CLASS="toptable" WIDTH="3PX"></TD>
								    <form name="leva_filtra" action="'.$_SERVER['PHP_SELF'].'" method="POST" target="_self">
								    <input type="hidden" name="fase" value="leva_filtri">
								    <TD CLASS="toptable"><input type="submit" class="bottone2" value="Rimuovi Filtri"></TD></form> 
								   </TR> 
						      </TABLE>
						     </TD> 
						   </TR>
							<!--------------------------------------- FINE QUARTA RIGA FILTRI -----------------------------------------------------------> 
						</TABLE>  	
							
						</TD>
					 </TR>
					</TABLE>	

	    </div> 
	    
	    
 	     <div id="corpo_sito">
	      <div id="content">';


// _______________________________________________________________________________________________________________ DISEGNO LA TABELLA
   if ($RIS_LETTURE){
	  	
	    echo '<table align="center" width="100%" cellpadding="1" cellspacing="0" >
	          <tr>
	           <td colspan=22>';
	           
	    Paginazione();       
	    
	  	echo'  </td>
	  	      </tr>
	  	      <tr>'; 
		  $classe="toptableletture";
		  fill_cella(1, '', 'center', $classe);
		  fill_cella(1, 'pdr', 'center', $classe);
		  fill_cella(1, 'Cod.Ut.', 'center', $classe);
		  fill_cella(1, 'Utente', 'center', $classe);
		  fill_cella(1, 'Indirizzo', 'center', $classe);
		  fill_cella(1, 'Civ.', 'center', $classe);
		  fill_cella(1, '/', 'center', $classe);
		  fill_cella(1, 'Localita', 'center', $classe);
		  fill_cella(1, 'Comune', 'center', $classe);
		  fill_cella(1, 'Matricola', 'center', $classe);
		  fill_cella(1, 'Let.Prec.', 'center', $classe);
		  fill_cella(1, 'Let.Min.', 'center', $classe);
		  fill_cella(1, 'LETTURA', 'center', $classe);
		  fill_cella(1, 'Let.Max.', 'center', $classe);
		  fill_cella(1, 'DATA', 'center', $classe);
		  fill_cella(1, 'Cons.', 'center', $classe);
		  fill_cella(1, 'Acc', 'center', $classe);
		  fill_cella(1, 'S1', 'center', $classe);
		  fill_cella(1, 'S2', 'center', $classe);
		  fill_cella(1, 'S3', 'center', $classe);
		  fill_cella(1, 'Fl', 'center', $classe);
		  fill_cella(1, '->', 'center', $classe);
		  echo'</tr>';
	  
	  
	  $indice=-1;
	  while ($riga=mysql_fetch_array($RIS_LETTURE)){
	   $indice=$indice+1;	
	   if ($indice % 2){$classe="cellagriglia";} else {$classe="cellagrigliabianca";}
	   //if ($riga['controllata']==1){$classe="rigacontrollata";}
	   //if ($riga['controllata']==1){$tmpFase="leva_spunta"; $imgspunta="spunta_verde.jpg";} else {$tmpFase="spunta"; $imgspunta="spunta_grigia.jpg";}
	   echo '
	    <td class="'.$classe.'" align="right" style="width:5px; height:5px;">
	    </td>'; 
	    
	   //$localita=GetLocalitaSmat($impianto, $giro, $riga["localita"]); 
	   $localita=$riga["localita"];
	    
	   fill_cella(2, 'pdr', 'center', $classe);
		 fill_cella(2, 'codice_utente', 'right', $classe);
	   fill_cella(2, 'utente', 'left', $classe);
	   fill_cella(2, 'indirizzo','left', $classe);
	   fill_cella(2, 'civico','left', $classe);
	   fill_cella(2, 'barrato', 'left', $classe);
	   fill_cella(1, $localita,'left', $classe);
	   fill_cella(2, 'comune','left', $classe);
	   fill_cella(2, 'matricola','right', $classe);
	   fill_cella(2, 'lettura_precedente_ente','right', $classe);
	   fill_cella(2, 'letmin','right', $classe);
	   fill_cella(2, 'lettura','right', $classe);
	   fill_cella(2, 'letmax','right', $classe);
	   fill_cella(2, 'data_lettura','right', $classe);
	   if (is_numeric($riga['lettura'])==true && is_numeric($riga['lettura_precedente_ente'])==true) {fill_cella(1,intval($riga['lettura'])-intval($riga['lettura_precedente_ente']), 'right', $classe);} else {fill_cella(1, '0', 'center', $classe);}
	   fill_cella(2, 'acc','center', $classe);
	   fill_cella(2, 'segn1','center', $classe);     
	   fill_cella(2, 'segn2','center', $classe);     
	   fill_cella(2, 'segn3','center', $classe);     
	   fill_cella(2, 'flag','center', $classe);     
	   
	   echo'
	    <td class="'.$classe.'" align="center">
	     <a class="piccolorossoblu" href="dettagli_lettura.php?indice='.$indice.'&totletture=0">
	     ';
	     if ($riga["flag_foto_1"]==1 || $riga["flag_foto_2"]==1 || $riga["flag_foto_3"]==1) {
	     	echo'<IMG SRC="images/photo.jpg" BORDER=0>';} else {echo'->';
	     } // if
	   echo'</a></td></tr>';   
	   } // While
	   echo'</table>';
	  } 
	
// __________________________________________________________________________________________________________________________________



require_once("footer.php");

function Paginazione(){
global $QUERY;
global $QUERY_EXTRA;
global $limite_inferiore_lavorazioni;
	
	$query="select count(*) as tot from letture where progressivo>$limite_inferiore_lavorazioni and stato in ('SPE') $QUERY $QUERY_EXTRA";
  $ris=mysql_query($query);
	$rs=mysql_fetch_array($ris);
	$parteintera = round($rs["tot"]/$_SESSION["righe_per_pagina"]);
	$numerocompleto = $rs["tot"]/$_SESSION["righe_per_pagina"];
	if ($numerocompleto>$parteintera) {$parteintera +=1;}
	$_SESSION["tot_pagine"]=$parteintera;
	$_SESSION["totali_filtrate"]=$rs["tot"];	
	
	echo'
	 <TABLE WIDTH="100%" BORDER="0">
	  <TR>
	   <TD ALIGN="center" >
	    <TABLE WIDTH="180" align="center" border="0">
	     <TR>
	';
	if ($_SESSION["pagina"]>1) {
    echo'<TD CLASS="toptable" align="center" valign="center"><A HREF="'.$pg.'?fase=pagina_prima"><IMG SRC="images/freccetta_inizio.jpg" BORDER="0PX"></A></TD>';
    echo'<TD CLASS="toptable" align="center" valign="center"><A HREF="'.$pg.'?fase=pagina_prec"><IMG SRC="images/freccetta_sx.jpg" BORDER="0PX"></A></TD>';
   } else {
   	echo'<TD CLASS="toptable" align="center" valign="center"><IMG SRC="images/freccetta_inizio_dis.jpg" BORDER="0PX"></TD>';
    echo'<TD CLASS="toptable" align="center" valign="center"><IMG SRC="images/freccetta_sx_dis.jpg" BORDER="0PX"></TD>';
   }
   echo'<TD CLASS="toptable" align="center" valign="center">&nbsp;'.$_SESSION["pagina"].' / '.$_SESSION["tot_pagine"].'&nbsp;</TD>';
   if ($_SESSION["pagina"]<$_SESSION["tot_pagine"]) {
    echo'<TD CLASS="toptable" align="center" valign="center"><A HREF="'.$pg.'?fase=pagina_succ"><IMG SRC="images/freccetta_dx.jpg" BORDER="0PX"></A></TD>';
    echo'<TD CLASS="toptable" align="center" valign="center"><A HREF="'.$pg.'?fase=pagina_ultima"><IMG SRC="images/freccetta_fine.jpg" BORDER="0PX"></A></TD>';
   } else {
   	echo'<TD CLASS="toptable" align="center" valign="center"><IMG SRC="images/freccetta_dx_dis.jpg" BORDER="0PX"></TD>';
    echo'<TD CLASS="toptable" align="center" valign="center"><IMG SRC="images/freccetta_fine_dis.jpg" BORDER="0PX"></TD>';
 }
 echo'  </TR>
       </TABLE>
      </TD>
     </TR>
    </TABLE>   '; 
	
}

function PaginazioneFile(){
global $CONDIZIONE_QUERY_FILE_TOP;
global $FROM_FILE_TOP;

  $qEye="";
  if ($_SESSION["vedi_file_checkati"]==0){$qEye=" and caricofile_checked=0 "; }
	
	$query="select b.caricofile_id $FROM_FILE_TOP $CONDIZIONE_QUERY_FILE_TOP $qEye group by b.caricofile_id";
	 //echo $query;
  $ris=mysql_query($query);
	$rs=mysql_fetch_array($ris);
	$buff=mysql_num_rows($ris);
	$parteintera = round($buff/$_SESSION["file_righe_per_pagina"]);
	$numerocompleto = $buff/$_SESSION["file_righe_per_pagina"];
	if ($numerocompleto>$parteintera) {$parteintera +=1;}
	$_SESSION["file_tot_pagine"]=$parteintera;
	
	echo'
	 <TABLE WIDTH="100%" BORDER="0">
	  <TR>
	   <TD>';
	   
	   //echo "TOT_PAG= ".$_SESSION["file_tot_pagine"]." FP=".$_SESSION["file_pagina"];
	   
	   echo'
	    </TD>
	  </TR>  
	  <TR>
	   <TD ALIGN="center" >
	    <TABLE WIDTH="180" align="center" border="0">
	     <TR>
	';
	if ($_SESSION["file_pagina"]>1) {
    echo'<TD CLASS="toptable" align="center" valign="center"><A HREF="'.$pg.'?fase=file_pagina_prima"><IMG SRC="images/freccetta_inizio.jpg" BORDER="0PX"></A></TD>';
    echo'<TD CLASS="toptable" align="center" valign="center"><A HREF="'.$pg.'?fase=file_pagina_prec"><IMG SRC="images/freccetta_sx.jpg" BORDER="0PX"></A></TD>';
   } else {
   	echo'<TD CLASS="toptable" align="center" valign="center"><IMG SRC="images/freccetta_inizio_dis.jpg" BORDER="0PX"></TD>';
    echo'<TD CLASS="toptable" align="center" valign="center"><IMG SRC="images/freccetta_sx_dis.jpg" BORDER="0PX"></TD>';
   }
   echo'<TD CLASS="toptable" align="center" valign="center">&nbsp;'.$_SESSION["file_pagina"].' / '.$_SESSION["file_tot_pagine"].'&nbsp;</TD>';
   if ($_SESSION["file_pagina"]<$_SESSION["file_tot_pagine"]) {
    echo'<TD CLASS="toptable" align="center" valign="center"><A HREF="'.$pg.'?fase=file_pagina_succ"><IMG SRC="images/freccetta_dx.jpg" BORDER="0PX"></A></TD>';
    echo'<TD CLASS="toptable" align="center" valign="center"><A HREF="'.$pg.'?fase=file_pagina_ultima"><IMG SRC="images/freccetta_fine.jpg" BORDER="0PX"></A></TD>';
   } else {
   	echo'<TD CLASS="toptable" align="center" valign="center"><IMG SRC="images/freccetta_dx_dis.jpg" BORDER="0PX"></TD>';
    echo'<TD CLASS="toptable" align="center" valign="center"><IMG SRC="images/freccetta_fine_dis.jpg" BORDER="0PX"></TD>';
 }
 echo'  </TR>
       </TABLE>
      </TD>
     </TR>
    </TABLE>   '; 
	
}
        

?>
