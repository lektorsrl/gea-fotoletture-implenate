<?php

// ****************************************************************************************************************************************************
//
// SOVRAIMPRIMI JPG
//
// La funzine SovraimprimiJPG scrive il testo_up ed il testo_down sulla foto con sfondo nero e testo verde e risalva l'immagine cos� modificata
// Occhio che in certi hosting linux l'immagine jpg deve essere raggiunta dal perorso ASSOLUTO /htdocs/public/www.....ecc
//
// ****************************************************************************************************************************************************


// ------------------------------------------------------- TEST FUNZIONAMENTO
//$text_up = "01/01/2014  16:38:39";
//$text_down = "Term= 001 ULM= 1234567890";
//$immagine="/htdocs/public/www/GEA_4/1.jpg";
//SovraimprimiJPG($text_up, $text_down, $immagine);
// --------------------------------------------------------------------------
  
function SovraimprimiJPG($text_up, $text_down, $immagine){


  //Set the Content Type
 // header('Content-type: image/jpeg');

  // Immagine
  $jpg_image = imagecreatefromjpeg($immagine);
  $jpg_width = imagesx($jpg_image);
  $jpg_height= imagesy($jpg_image);

  // Colori
  $verde= imagecolorallocate($jpg_image, 37, 228, 60);
  $rosso= imagecolorallocate($jpg_image, 255, 0, 0);
  $nero= imagecolorallocate($jpg_image, 0, 0, 0);

  // Font e testo
  //$font_path = '/htdocs/public/www/fotoletture/tahoma.ttf'; 
  $font_path = '/htdocs/public/www/fotoletture/tahoma.ttf';
  $font_size = 14;
  
  
  
  // ___________________________________________________________________________ TESTO SOPRA
  
  $type_space = imagettfbbox($font_size, 0, $font_path, $text_up);
  $text_width = abs($type_space[4] - $type_space[0])+10;
	$text_height = abs($type_space[5] - $type_space[1])+10;
	
	// Coordinate
	$x_up = $jpg_width-$text_width;
	$y_up = 0;
	
	//die("testo= $text_up  x=$x_up   y=$y_up   jpgW=$jpg_width   jpgH=$jpg_height   texth=$text_height ");

  
  // Rettanglo Nero
  if (imagefilledrectangle($jpg_image, $x_up-10, $y_up, $jpg_width, $text_height+5, $nero)){

	  // Testo Verde
	  imagettftext($jpg_image, $font_size, 0, $x_up, $y_up+26, $verde, $font_path, $text_up);
	  
	}  
  
  
  
  
  // ___________________________________________________________________________ TESTO SOTTO
  
  $type_space = imagettfbbox($font_size, 0, $font_path, $text_down);
  $text_width = abs($type_space[4] - $type_space[0])+10;
	$text_height = abs($type_space[5] - $type_space[1])+10;
	
	// Coordinate
	$x_down = $jpg_width-$text_width;
	$y_down = $jpg_height-$text_height;

  
  // Rettanglo Nero
  if (imagefilledrectangle($jpg_image, $x_down, $y_down, $jpg_width, $jpg_height, $nero)){

    // Testo Verde
    imagettftext($jpg_image, $font_size, 0, $x_down, $y_down+23, $verde, $font_path, $text_down);
    
  }  
  
  
  

  // Send Image to Browser
  //imagejpeg($jpg_image); //Scrive su browser
  
  imagejpeg($jpg_image, $immagine);
  

  // Clear Memory
  imagedestroy($jpg_image);
  
  
}  
  
  
?>
