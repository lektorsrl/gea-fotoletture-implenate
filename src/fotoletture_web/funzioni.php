<?php

require_once("pclzip.lib.php");


if (!isset($_SESSION["connesso"])){
  
  $_SESSION["connesso"]="no";
  
  
  // Utilizzatore
  $_SESSION["ses-ut-nome"]="";
 	$_SESSION["ses-ud-id"]=0;
 	$_SESSION["ses-ut-ruolo"]=3;
 	$_SESSION["ses-ut-sede"]=1;
 	$_SESSION["ses-ut-ente"]=5;
 	$_SESSION["ses-connesso"]="no";
 	$_SESSION["ses-ut-sede-db"]= "???";
	$_SESSION["ses-ut-ditta"]= "";
	$_SESSION["ses-ut-sede"]= "???"; 
	$_SESSION["ses-ut-enti_per_query"]= ""; 
	
	$_SESSION["ses_letturisti"]="";
	
	
 	// Query Ente-Sede
 	$_SESSION["ses-query_ente_sede"]="";
 	$_SESSION["ses-query_ente_file"];
 	
  
  // Percorsi
  $_SESSION["ses-dirfoto"]="foto/";
  
  // Gestione Pagine
  $_SESSION["ses-pagina"]=1;
  $_SESSION["ses-righe_per_pagina"]=30;
  $_SESSION["ses-tot_pagine"]=0;
  $_SESSION["ses-sequenza"]="1";
  
  
  // Trasporto query tra pagine
  $_SESSION["ses-query"]="";
  
  // georeferenziazione
  $_SESSION["ses-tipomappa"]="";
  $_SESSION["ses-numpunti"]=1;
  $_SESSION["ses-latitudini"]="";
  $_SESSION["ses-longitudini"]="";
  $_SESSION["ses-nomi"]="";
  $_SESSION["ses-indirizzi"]="";
  $_SESSION["ses-tipi"]="";
  $_SESSION["ses-zoom"]="";
  $_SESSION["ses-larghezza"]="";
  $_SESSION["ses-altezza"]="";
  $_SESSION["ses-controlli"]="";
	$_SESSION["ses-comune"]="";
	$_SESSION["ses-file"]="";
	$_SESSION["localita"]="";
	
	// Altro
	$_SESSION["ses-mostra_download_foto"]=1;

}

require_once("config/config.php");
 
 
// Apro il db System e vedo se c'� almeno una lavorazione
//------------------------------------------------------------------------------------------------------------------------------------------- 
Apri_DB("system");

//Controllo che ci sia almeno una lavorazione altrimento blocco tutto
$query="select * from lavorazione";
$ris = mysql_query($query);
if (mysql_num_rows($ris)==0){
	include("no_dati.php");
	die("");
}


$ris=mysql_query("select * from letturista", $connessione);
$buff="";
while ($rs=mysql_fetch_array($ris)){
	$i=$rs["letturista_numero"];
	$Letturisti[$i]=$rs["letturista_nome"];
}
//------------------------------------------------------------------------------------------------------------------------------------------- 


// Apro il DB Fotoletture
//------------------------------------------------------------------------------------------------------------------------------------------- 
Apri_DB("fotoletture");
//------------------------------------------------------------------------------------------------------------------------------------------- 


if(!function_exists('IsDateDodo')) {
	function IsDateDodo($stringa) {
	  
	  $d = substr($stringa, 0, 2);
	  $m = substr($stringa, 3, 2);
	  $y = substr($stringa, 6, 4);
	  return checkdate ($m, $d, $y);
	}
}	

function IsDate($string) {
  $t = strtotime($string);
  $m = date('m',$t);
  $d = date('d',$t);
  $y = date('Y',$t);
  //DEBUG ("time",$t);
  //DEBUG ("mese",$m);
  //DEBUG ("giorno",$d);
  //DEBUG ("anno",$y);
  return checkdate ($m, $d, $y);
}

function FormatStr($s, $tipo, $lung){
 $stringazza="                                                                                                                                                                ";
 $numerazzo="00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000";
 $risultato=$s;
 switch ($tipo){
  case "SD": $risultato = substr($s.$stringazza, 0, $lung); break;
  case "SS": $risultato = substr($stringazza.$s, (-1)*$lung); break;
  case "N":  $risultato = substr($numerazzo.$s, (-1)*$lung); break;
 }
 return $risultato;
}


function data_diff($interval, $date1, $date2) { 
// date1 � la pi� vecchia
// date2 � la pi� giovane
    $seconds = $date2 - $date1; 
    switch ($interval) { 
        case "y":    // years 
            list($year1, $month1, $day1) = split('-', date('Y-m-d', $date1)); 
            list($year2, $month2, $day2) = split('-', date('Y-m-d', $date2)); 
            $time1 = (date('H',$date1)*3600) + (date('i',$date1)*60) + (date('s',$date1)); 
            $time2 = (date('H',$date2)*3600) + (date('i',$date2)*60) + (date('s',$date2)); 
            $diff = $year2 - $year1; 
            if($month1 > $month2) { 
                $diff -= 1; 
            } elseif($month1 == $month2) { 
                if($day1 > $day2) { 
                    $diff -= 1; 
                } elseif($day1 == $day2) { 
                    if($time1 > $time2) { 
                        $diff -= 1; 
                    } 
                } 
            } 
            break; 
        case "m":    // months 
            list($year1, $month1, $day1) = split('-', date('Y-m-d', $date1)); 
            list($year2, $month2, $day2) = split('-', date('Y-m-d', $date2)); 
            $time1 = (date('H',$date1)*3600) + (date('i',$date1)*60) + (date('s',$date1)); 
            $time2 = (date('H',$date2)*3600) + (date('i',$date2)*60) + (date('s',$date2)); 
            $diff = ($year2 * 12 + $month2) - ($year1 * 12 + $month1); 
            if($day1 > $day2) { 
                $diff -= 1; 
            } elseif($day1 == $day2) { 
                if($time1 > $time2) { 
                    $diff -= 1; 
                } 
            } 
            break; 
       case "w":    // weeks 
            // Only simple seconds calculation needed from here on 
            $diff = floor($seconds / 604800); 
            break; 
       case "d":    // days 
            $diff = floor($seconds / 86400); 
            break; 
       case "h":    // hours 
            $diff = floor($seconds / 3600); 
            break; 
       case "i":    // minutes 
            $diff = floor($seconds / 60); 
            break; 
       case "s":    // seconds 
            $diff = $seconds; 
            break; 
    } 
    return $diff; 
}

function flash($width, $height, $nome){
 echo'
  <OBJECT classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,0,0" WIDTH="'.$width.'" HEIGHT="'.$height.'" id="'.$nome.'" ALIGN="">
   <PARAM NAME=movie VALUE="'.$nome.'"> 
   <PARAM NAME=quality VALUE=high> 
   <PARAM NAME=bgcolor VALUE=#FFFFFF> 
   <EMBED src="'.$nome.'" quality=high bgcolor=#FFFFFF  WIDTH="'.$width.'" HEIGHT="'.$height.'" NAME="'.$nome.'" ALIGN="" TYPE="application/x-shockwave-flash" PLUGINSPAGE="http://www.macromedia.com/go/getflashplayer"></EMBED>
  </OBJECT>
 ';
}



function StripTrim($s){
	return trim(str_replace("'","''",stripslashes($s)));
}


function GetPercorsoFotoFTP($InitPath, $progressivo, $sequenza, &$percorso){
	$percorso=$InitPath;
	if(!is_dir($percorso)) {mkdir($percorso, 0777);}
	$percorso.="lav_".formatStr($progressivo, "N", 6)."/";
	if(!is_dir($percorso)) {mkdir($percorso, 0777);}
	$progressivo=formatStr($progressivo, "N", 6);
	$sequenza=formatStr($sequenza, "N", 6);
	//$nomeFile=$progressivo."_".$sequenza."_".$id_foto."_";
	for ($i=1; $i<=4; $i++){
		$cartella=substr($sequenza, $i-1, 1).substr("000000", 0, 6-$i);
		$percorso.=$cartella."/";
		if(!is_dir($percorso)) {mkdir($percorso , 0777);}
	}
}

function CLEAN_VAR($buff){
	$buff=str_replace(",", "", $buff);
	$buff=str_replace(".", "", $buff);
	$buff=str_replace("(", "", $buff);
	$buff=str_replace(")", "", $buff);
	$buff=str_replace("'", "", $buff);
	$buff=str_replace(chr(34), "", $buff);
	$buff=str_replace("+", "", $buff);
	$buff=str_replace("/", "", $buff);
	$buff=str_replace("*", "", $buff);
	return $buff;
}




?>
