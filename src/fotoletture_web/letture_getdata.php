<?php

  session_start();
	require_once("funzioni.php");
	
	Apri_DB("sede");

	$page = isset($_POST['page']) ? intval($_POST['page']) : 1;
	$rows = isset($_POST['rows']) ? intval($_POST['rows']) : 14;
	$caricofile_id = isset($_POST['caricofile_id']) ? mysql_real_escape_string($_POST['caricofile_id']) : '';
	
	$pdr = isset($_POST['pdr']) ? mysql_real_escape_string($_POST['pdr']) : '';
	$codice_utente = isset($_POST['codice_utente']) ? mysql_real_escape_string($_POST['codice_utente']) : '';
	$utente = isset($_POST['utente']) ? mysql_real_escape_string($_POST['utente']) : '';
	$matricola = isset($_POST['matricola']) ? mysql_real_escape_string($_POST['matricola']) : '';
	$segnalazione = isset($_POST['segnalazione']) ? mysql_real_escape_string($_POST['segnalazione']) : '';
	$acc = isset($_POST['acc']) ? mysql_real_escape_string($_POST['acc']) : '';
	$flag = isset($_POST['flag']) ? mysql_real_escape_string($_POST['flag']) : '';
	$caricofile_id= isset($_POST['caricofile_id']) ? mysql_real_escape_string($_POST['caricofile_id']) : '';
	$localita = isset($_POST['localita']) ? mysql_real_escape_string($_POST['localita']) : '';
	
	$query_ente_file=$_SESSION["ses-query_ente_file"];
	$query_ente_sede=$_SESSION["ses-query_ente_sede"];
	
	$offset = ($page-1)*$rows;
	$result = array();
	
	$ListaCampi="id,extra,lavorazione_id,appalto_id,chiave_univoca,progressivo,sequenza,stato,carico,terminale,pdr,zona,libro,pagina,codice_utente,utente,utente_nuovo,ordine_vie,ordine_civici,ordine_utenti,toponomastica,indirizzo,indirizzo_nuovo,civico,civico_nuovo,barrato,barrato_nuovo,scala,piano,interno,localita,localita_nuova,comune,comune_nuovo,cap,provincia,progmis,periodicita,fornitura,misuratore,matricola,matricola_nuova,cifre,cifre_nuove,cifre_correttore,cifre_correttore_nuove,anno,anno_nuovo,acc,acc_nuova,locazione,locazione_nuova,classe,classe_nuova,categoria_merceologica,categoria_merceologica_nuova,fabbricante,fabbricante_nuovo,modello,modello_nuovo,marca,marca_nuova,matricola_correttore,matricola_correttore_nuova,tipo_quadrante,tipo_quadrante_nuovo,portata,portata_nuova,note,note_nuove,ubicazione,ubicazione_nuova,stato_contatore,lettura_precedente_ente,data_lettura_precedente_ente,lettura_precedente_ditta,data_lettura_precedente_ditta,lettura_precedente_correttore,consumo_annuo,consumo_gg,letmin,letmax,data_primo_passaggio,ora_primo_passaggio,data_lettura,ora_lettura,lettura,lettura_correttore,lettura_corretta,consumo,segn1,segn1_causa,segn2,segn2_causa,segn3,segn3_causa,flag,nota,tentativi,flag_foto_1,flag_foto_2,flag_foto_3,oltre_al_max,delta_tempo,editata_da_programma,numero_satelliti,latitudine,longitudine,indice_file,codice_ente,numero_invio,autolettura,lettura_eseguita,data_reale_lettura,flag_controllata,censimenti,numero_invio_autoletture,profilo_di_prelievo,base_di_computo,ANNOMESE_STIPENDIO,tipo_rilevazione,data_inizio,data_fine";
	
	// ______________________________________________________________________________________________ QUERY WHERE
	$vigileArchivio=0;
	$queryWhere="where 1 $query_ente_sede";
	if (strlen($caricofile_id)>0){$queryWhere.=" and indice_file=$caricofile_id ";}
	if (strlen($pdr)>0){$queryWhere.=" and pdr=$pdr "; $vigileArchivio=1;}
	if (strlen($codice_utente)>0){$queryWhere.=" and codice_utente=$codice_utente "; $vigileArchivio=1;}
	if (strlen($utente)>0){$queryWhere.=" and utente like '%$utente%' "; $vigileArchivio=1;}
	if (strlen($matricola)>0){$queryWhere.=" and substring(matricola, -".strlen($matricola).")='$matricola' "; $vigileArchivio=1;}
	if (strlen($localita)>0){$queryWhere.=" and localita='$localita' ";}
	if (strlen($segnalazione)>0){$queryWhere.=" and segn1='$segnalazione' ";}
	if (strlen($acc)>0){$queryWhere.=" and acc='$acc' ";}
	if (strlen($flag)>0){$queryWhere.=" and flag='$flag' ";}
	
	Writelog("letture_getdata, query ente sede: ".$query_ente_sede);

	//Aggiunta per far vedere le letture validate con la O in gea4 e allo stato LET
	//if (strpos($query_ente_sede, '23')){$queryWhere.=" and flag_controllata='O' ";}
	//if (strpos($query_ente_sede, '24')){$queryWhere.=" and flag_controllata='O' ";}

	// ______________________________________________________________________________________________ QUERY FROM
	$vigileArchivio=0;
	if ($vigileArchivio==0){
		$QUERY_LETTURE= "select * from letture $queryWhere order by progressivo desc limit $offset,$rows";
		$QUERY_CONTA="select count(*) from letture $queryWhere";
	} else {
		$QUERY_LETTURE= "(select $ListaCampi from letture $queryWhere) union (select $ListaCampi from letture_archivio $queryWhere)"; 
		$QUERY_CONTA="select count(*) from $QUERY_LETTURE ";
		$QUERY_LETTURE= $QUERY_LETTURE." order by progressivo desc limit $offset,$rows"; 
	}

	$rs = mysql_query($QUERY_CONTA);
		 if (!$rs) { // add this check.
    			die('Errore query: ' . mysql_error());
		 }
	$row = mysql_fetch_row($rs);
	$result["total"] = $row[0];
	
	
	//$myfile = fopen("logs.txt", "a") or die("Unable to open file!");
	//fwrite($myfile, "\n"."$QUERY_LETTURE");
	//fclose($myfile);
	
	
	//WriteLog($QUERY_LETTURE);
	          
	//die($QUERY_FILE);              
	// LE RIGHE SUCCESSIVE SONO STATE TOLTE PER EVITARE IL PROBLEMA DEI CARATTERI SPECIALI             
	//$rs = mysql_query($QUERY_LETTURE);
	
	//$rows = array();
	//while($row = mysql_fetch_object($rs)){
	//	array_push($rows, $row);
	//}
	//$result["rows"] = $rows;
	
	//echo json_encode($result);


// Aggiunta da Walter il 21/12/2016 per risolvere il problema dei caratteri speciali
function codifica_utf8(&$item1, $key, $prefix){
  $item1=utf8_encode((!is_null($item1))?$item1:"");
}

	             
	$rs = mysql_query($QUERY_LETTURE);
	
	$rows = array();
	while($row = mysql_fetch_object($rs)){
		// Aggiunta da Walter il 21/12/2016 per risolvere il problema dei caratteri speciali
		array_walk($row,'codifica_utf8');
		WriteLog("SONO DENTRO");
		array_push($rows, $row);
	}
	$result["rows"] = $rows;
	

	echo json_encode($result);
	
	function WriteLog($s){
		$myfile = fopen("logs.txt", "a");
	  fwrite($myfile, $s);
		fclose($myfile);
	}



?>