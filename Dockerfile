FROM php:5.4-apache
RUN echo "deb http://packages.cloud.google.com/apt gcsfuse-jessie main" | tee /etc/apt/sources.list.d/gcsfuse.list; curl https://packages.cloud.google.com/apt/doc/apt-key.gpg | apt-key add - \
 && apt-get update && apt-get install -y gcsfuse libfreetype6-dev libjpeg-dev libpng-dev nano \
 && docker-php-ext-configure gd --with-freetype-dir --with-jpeg-dir --with-png-dirr \
 && docker-php-ext-install mysql mysqli gd zip
COPY --chown=www-data:www-data src /var/www/html
COPY docker/php/php.ini /usr/local/etc/php/php.ini